import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Alert, Modal } from "antd";
import AlertComponent from "../../Components/Alert/index";
import "../../App.css";
import { useMediaQuery } from "react-responsive";
import { productWishlist } from "../../stores/actions/history";
import { editStatusOfferIsAccepted, editStatusOfferIsNotSold, editStatusOfferIsRejected, editStatusOfferIsSold, getOfferById } from "../../stores/actions/offer";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import moment from "moment";
import LoadingWithBackground from "../../Components/Loading/withBackground";

function InfoPenawarPage() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalStatusVisible, setIsModalStatusVisible] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [isModalRejectedVisible, setIsModalRejectedVisible] = useState(false);

  const dispatch = useDispatch();
  const { id } = useParams();
  const { getOfferResult, getEditStatusOfferIsRejectedResult, getEditStatusOfferIsAcceptedResult, getEditStatusOfferIsSoldResult, getOfferLoading, getEditStatusOfferisNotSoldResult } = useSelector((state) => state.offerReducer);
  const token = JSON.parse(localStorage.getItem("userLocal")).accessToken;

  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  useEffect(() => {
    dispatch(productWishlist({ token }));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getOfferById({ id, token }));
  }, [dispatch, id]);

  const showModal = () => {
    setIsModalVisible(true);
    dispatch(editStatusOfferIsAccepted({ id, status: "accepted", token }));
  };

  const handleOk = async () => {
    const res = await axios.get(`https://secondhand-restapi.herokuapp.com/api/offer/show-offer/whastapp/${id}`, {
      headers: {
        accept: "*/*",
        Authorization: `Bearer ${token}`,
      },
    });
    setIsModalVisible(false);
    window.open(res.data, "_blank")?.focus();
    window.location.reload();
  };

  // rejected handler modal
  const showModalRejected = () => {
    setIsModalRejectedVisible(true);
  };

  const handleOkRejected = () => {
    dispatch(editStatusOfferIsRejected({ id, status: "rejected", token }));
    setIsModalRejectedVisible(false);
    {
      <>
        <div className="hidden">
          {setTimeout(function () {
            window.location.href = "/daftar-jual/terjual";
          }, 3000)}
        </div>
        <Alert message={"Status Produk Berhasil Diperbarui"} />
      </>;
    }
  };

  const handleCancelRejected = () => {
    setIsModalRejectedVisible(false);
  };
  // end rejected handler modal

  const handleOkStatus = () => {
    setIsModalStatusVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCancelStatus = () => {
    setIsModalStatusVisible(false);
  };

  const handleStatusSubmit = (event) => {
    event.preventDefault();
    const status = document.querySelector('input[name="status_penjualan"]:checked').value;
    if (status === "berhasil-terjual") {
      handleCheckedSuccess();
    } else if (status === "batal-terjual") {
      handleCheckedFailed();
    }
    setIsModalStatusVisible(false);
  };

  const handleChecked = () => {
    setIsChecked(true);
  };

  const handleCheckedSuccess = () => {
    dispatch(editStatusOfferIsSold({ id, status: "sold", token }));
    {
      <>
        <div className="hidden">
          {window.setTimeout(() => {
            window.location.href = "/daftar-jual/terjual";
          }, 3000)}
        </div>
        <Alert message={"Silahkan Login atau lengkapi profile terlebih dahulu !!"} />
      </>;
    }
  };

  const handleCheckedFailed = () => {
    dispatch(editStatusOfferIsNotSold({ id, status: "notSold", token }));
    {
      <>
        <div className="hidden">
          {window.setTimeout(() => {
            window.location.href = "/daftar-jual/terjual";
          }, 3000)}
        </div>
        <Alert message={"Silahkan Login atau lengkapi profile terlebih dahulu !!"} />
      </>;
    }
  };

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1024px)",
  });

  const isTabletOrMobile = useMediaQuery({ query: "(max-width:640px)" });
  return (
    <>
      {getEditStatusOfferIsRejectedResult && <Alert message={"Status Produk Berhasil Diperbaharui"} />}
      {getEditStatusOfferIsAcceptedResult && <Alert message={"Status Produk Berhasil Diperbaharui"} />}
      {getEditStatusOfferIsSoldResult && <Alert message={"Status Produk Berhasil Diperbaharui"} />}
      {getEditStatusOfferisNotSoldResult && <Alert message={"Status Produk Berhasil Diperbaharui"} />}
      {getOfferLoading && <LoadingWithBackground />}
      {isTabletOrMobile ? (
        <div className="py-4 flex items-center">
          <Link to="/notifikasi" className="absolute left-0">
            <img src="/assets/images/fi_arrow-left.svg" alt="<" className="pl-4" />
          </Link>
          <span className="grow text-center font-semibold text-sm">Info Penawar</span>
        </div>
      ) : (
        <div className="flex flex-row items-center px-28 py-4 w-full h-full shadow">
          <Link to={"/"}>
            <img src="/assets/images/tilas.jpg" alt="logo" className="h-12 w-14 object-cover" />
          </Link>
          <span className="mx-auto text-base items-center font-medium">Info Penawar</span>
        </div>
      )}

      {isDesktopOrLaptop ? (
        <div className="w-2/5 h-full mx-auto mt-6">
          <div className="flex items-start">
            <div className="mr-20">
              <Link to={"/daftar-jual/semua"}>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M19 12H5" stroke="#151515" strokeWidth="2" stroke-linecap="round" stroke-linejoin="round" />
                  <path d="M12 19L5 12L12 5" stroke="#151515" strokeWidth="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
              </Link>
            </div>
            {/* Card Pembeli */}
            <div className="h-full w-full flex shadow py-4 pl-4 rounded-md">
              <img src={getOfferResult.imageProfil} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
              <div className="flex flex-col self-center">
                <p className="text-body-14 font-semibold ml-4">
                  {getOfferResult ? getOfferResult.fullname : "Nama Pembeli"} <span className="block text-body-10 mt-1 text-neutral03 font-medium">{getOfferResult.city}</span>
                </p>
              </div>
            </div>
            {/* End Card Pembeli */}
          </div>
          <div className="flex flex-col ml-24">
            <p className="text-body-14 font-medium my-6">Daftar Produkmu yang Ditawar</p>
            {/* Card Penawaran Produk */}
            <div className="h-full w-full flex-col rounded-md shadow py-4 pl-4 ">
              <div className="flex">
                <img src={getOfferResult.image1} alt={getOfferResult.productName} className="w-12 h-12 object-cover rounded-lg" />
                <div className="flex flex-col">
                  <span className="text-body-10 ml-4 text-neutral03 ">{getEditStatusOfferIsSoldResult === "Product status updated successfully" ? "Berhasil Terjual" : "Penawaran produk"}</span>
                  <span className="text-body-14 ml-4 text-black">{getOfferResult.productName}</span>
                  <span className="text-body-14 ml-4 text-black">Rp {getOfferResult.price}</span>
                  <span className="text-body-14 ml-4 text-black ">Ditawar Rp {getOfferResult.priceNegotiated}</span>
                </div>
                <span className="text-body-10 text-neutral03 ml-auto mr-4">{moment(getOfferResult.createdAt).format("DD MMMM h:mm")}</span>
              </div>

              {getEditStatusOfferIsSoldResult === "Product status updated successfully" ? (
                <div className="mt-4 mr-4 justify-end hidden">
                  {getOfferResult.statusProcess === "ACCEPTED" ? (
                    <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={handleOkStatus}>
                      Status
                    </button>
                  ) : (
                    <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={showModalRejected}>
                      Tolak
                    </button>
                  )}
                  {getOfferResult.statusProcess === "ACCEPTED" ? (
                    <button className="px-8 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                      <div className="flex justify-evenly">
                        <p className="my-2">Hubungi di</p>
                        <span className="mt-[1.5vh]">
                          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                              d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                              fill="white"
                            />
                          </svg>
                        </span>{" "}
                      </div>
                    </button>
                  ) : (
                    <button className="px-12 py-1.5 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                      Terima
                    </button>
                  )}
                </div>
              ) : (
                <div className="mt-4 mr-4 flex justify-end">
                  {getOfferResult.statusProcess === "ACCEPTED" ? (
                    <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={handleOkStatus}>
                      Status
                    </button>
                  ) : (
                    <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={showModalRejected}>
                      Tolak
                    </button>
                  )}
                  {getOfferResult.statusProcess === "ACCEPTED" ? (
                    <button className="px-8 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                      <div className="flex justify-evenly">
                        <p className="my-2">Hubungi di</p>
                        <span className="mt-[1.5vh]">
                          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                              d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                              fill="white"
                            />
                          </svg>
                        </span>{" "}
                      </div>
                    </button>
                  ) : (
                    <button className="px-12 py-1.5 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                      Terima
                    </button>
                  )}
                </div>
              )}

              {/* Modal  Button Terima*/}
              <Modal visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={360} footer={null}>
                <p className="text-body-14 font-medium mt-8">Yeay kamu berhasil mendapat harga yang sesuai</p>
                <p className="text-body-14 text-neutral03 font-light leading-relaxed">Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</p>
                <div className="flex flex-col px-4 bg-[#EEEEEE] rounded-xl mb-6 py-2">
                  <p className="text-body-14 font-semibold text-center my-2">Product Match</p>
                  {/* Card Pembeli */}
                  <div className="h-full w-full flex py-2 pl-4 rounded-md">
                    <img src={getOfferResult.imageProfil} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                    <div className="flex flex-col self-center">
                      <p className="text-body-14 font-semibold ml-4">
                        {getOfferResult.fullname} <span className="block text-body-10 mt-1 text-neutral03 font-medium">{getOfferResult.city}</span>
                      </p>
                    </div>
                  </div>
                  {/* End Card Pembeli */}

                  {/* Card Pembeli */}
                  <div className="h-full w-full flex pl-4 rounded-md">
                    <img src={getOfferResult.image1} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                    <div className="flex flex-col">
                      <span className="text-body-14 ml-4 text-black">{getOfferResult.productName}</span>
                      <span className="text-body-14 ml-4 text-black line-through">Rp {getOfferResult.price}</span>
                      <span className="text-body-14 ml-4 text-black ">Ditawar Rp {getOfferResult.priceNegotiated}</span>
                    </div>
                  </div>
                  {/* End Card Pembeli */}
                </div>
                <button onClick={handleOk} className=" rounded-2xl w-full text-white text-body-14 bg-primary-darkblue04 py-1">
                  <div className="flex justify-evenly">
                    <p className="my-2">Hubungi via Whatsapp</p>
                    <span className="mt-[1.5vh]">
                      <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                          fill="white"
                        />
                      </svg>
                    </span>
                  </div>
                </button>
              </Modal>
              {/* End Modal Button Terima */}

              {/* Modal Button Status */}
              <Modal visible={isModalStatusVisible} onOk={handleOkStatus} onCancel={handleCancelStatus} width={360} footer={null}>
                <p className="text-body-14 font-semibold mt-6">Perbarui status penjualan produkmu</p>
                <form onSubmit={handleStatusSubmit}>
                  <input type="radio" id="success" name="status_penjualan" value="berhasil-terjual" onChange={handleChecked} className="appearance-none checked:bg-primary-darkblue04 cursor-pointer" />
                  <label htmlFor="success" className="text-body-14 font-medium inline-block ml-4">
                    Berhasil terjual
                  </label>
                  <p className="text-body-14 text-neutral03 ml-8 inline-block mt-2">Kamu telah sepakat menjual produk ini kepada pembeli</p>
                  <br />
                  <input type="radio" id="cancel" name="status_penjualan" value="batal-terjual" onChange={handleChecked} className="appearance-none checked:bg-primary-darkblue04 cursor-pointer" />
                  <label htmlFor="cancel" className="text-body-14 font-medium inline-block ml-4">
                    Batalkan transaksi
                  </label>
                  <p className="text-body-14 text-neutral03 ml-8 inline-block mt-2 mb-6">Kamu membatalkan transaksi produk ini dengan pembeli</p>
                  {isChecked ? (
                    <button type="submit" className=" rounded-2xl w-full text-white text-body-14 bg-primary-darkblue04 py-1">
                      <p className="my-2">Kirim</p>
                    </button>
                  ) : (
                    <button type="submit" className=" rounded-2xl w-full text-white text-body-14 bg-neutral03 py-1" disabled={true}>
                      <p className="my-2">Kirim</p>
                    </button>
                  )}
                </form>
              </Modal>
              {/* End Modal Button Status */}
            </div>
            {/* End Card Penawaran Produk */}
            {/* Modal Button Tolak */}
            <Modal title={""} visible={isModalRejectedVisible} onOk={handleOkRejected} onCancel={handleCancelRejected}>
              <p>Apakah anda yakin ingin menolak ?</p>
            </Modal>
            {/* End Modal Button Tolak */}
          </div>
        </div>
      ) : (
        // Mobile Responsive
        <div className="flex-col mx-4">
          <div className="h-full flex shadow  py-4 rounded-md  mt-6">
            <img src={getOfferResult.imageProfil} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg ml-4 " />
            <div className="flex flex-col self-center">
              <p className="text-body-14 font-semibold ml-4">
                {getOfferResult.fullname} <span className="block text-body-10 mt-1 text-neutral03 font-medium">{getOfferResult.city}</span>
              </p>
            </div>
          </div>
          <p className="text-body-14 my-6 font-semibold">Daftar Produkmu yang Ditawar</p>

          <div className="h-full w-full flex-col py-4 pl-4 ">
            <div className="flex">
              <img src={getOfferResult.image1} alt={getOfferResult.image1} className="w-12 h-12 object-cover rounded-lg" />
              <div className="flex flex-col">
                <span className="text-body-10 ml-4 text-neutral03 ">{getEditStatusOfferIsSoldResult === "Product status updated successfully" ? "Berhasil Terjual" : "Penawaran produk"}</span>
                <span className="text-body-14 ml-4 text-black">{getOfferResult.productName}</span>
                <span className="text-body-14 ml-4 text-black">Rp {getOfferResult.price}</span>
                <span className="text-body-14 ml-4 text-black ">Ditawar Rp {getOfferResult.priceNegotiated}</span>
              </div>
              <span className="text-body-10 text-neutral03 ml-auto mr-4">{moment(getOfferResult.createdAt).format("DD MMMM h:mm")}</span>
            </div>

            {getEditStatusOfferIsSoldResult === "Product status updated successfully" ? (
              <div className="mt-4 mr-4 justify-end hidden">
                {getOfferResult.statusProcess === "ACCEPTED" ? (
                  <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={handleOkStatus}>
                    Status
                  </button>
                ) : (
                  <button className="px-12 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={showModalRejected}>
                    Tolak
                  </button>
                )}
                {getOfferResult.statusProcess === "ACCEPTED" ? (
                  <button className="px-8 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                    <div className="flex justify-evenly">
                      <p className="my-2">Hubungi di</p>
                      <span className="mt-[1.5vh]">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                            fill="white"
                          />
                        </svg>
                      </span>{" "}
                    </div>
                  </button>
                ) : (
                  <button className="px-12 py-1.5 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                    Terima
                  </button>
                )}
              </div>
            ) : (
              <div className="mt-4 mr-4 flex justify-around">
                {getOfferResult.statusProcess === "ACCEPTED" ? (
                  <button className="px-14 py-1.5 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={handleOkStatus}>
                    Status
                  </button>
                ) : (
                  <button className="px-14 py-2 rounded-full text-black text-body-14 mr-4 font-medium border-2 border-primary-darkblue04" onClick={showModalRejected}>
                    Tolak
                  </button>
                )}
                {getOfferResult.statusProcess === "ACCEPTED" ? (
                  <button className="px-12 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                    <div className="flex justify-evenly">
                      <p className="my-2">Hubungi</p>
                      <span className="mt-[1.5vh]">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                            fill="white"
                          />
                        </svg>
                      </span>{" "}
                    </div>
                  </button>
                ) : (
                  <button className="px-14 py-2 rounded-full text-white text-body-14 font-medium border-2 bg-primary-darkblue04" onClick={showModal}>
                    Terima
                  </button>
                )}
              </div>
            )}

            {/* Modal  Button Terima*/}

            <Modal visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={null}>
              <p className="text-body-14 font-semibold mt-8">Yeay kamu berhasil mendapat harga yang sesuai</p>
              <p className="text-body-14 text-neutral03 font-light leading-relaxed">Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</p>
              <div className="flex flex-col px-4 bg-[#EEEEEE] rounded-xl mb-6 py-2">
                <p className="text-body-14 font-semibold text-center my-2">Product Match</p>
                {/* Card Pembeli */}
                <div className="h-full w-full flex py-2 pl-4 rounded-md">
                  <img src={getOfferResult.imageProfil} alt={getOfferResult.fullname} className="w-12 h-12 object-cover rounded-lg" />
                  <div className="flex flex-col self-center">
                    <p className="text-body-14 font-semibold ml-4">
                      {getOfferResult.fullname} <span className="block text-body-10 mt-1 text-neutral03 font-medium">{getOfferResult.city}</span>
                    </p>
                  </div>
                </div>
                {/* End Card Pembeli */}
                {/* Card Pembeli */}
                <div className="h-full w-full flex pl-4 rounded-md">
                  <img src={getOfferResult.image1} alt={getOfferResult.productName} className="w-12 h-12 object-cover rounded-lg" />
                  <div className="flex flex-col">
                    <span className="text-body-14 ml-4 text-black">{getOfferResult.productName}</span>
                    <span className="text-body-14 ml-4 text-black line-through">Rp {getOfferResult.price}</span>
                    <span className="text-body-14 ml-4 text-black ">Ditawar Rp {getOfferResult.priceNegotiated}</span>
                  </div>
                </div>
                {/* End Card Pembeli */}
              </div>
              <button onClick={handleOk} className=" rounded-2xl w-full text-white text-body-14 bg-primary-darkblue04 py-1">
                <div className="flex justify-evenly">
                  <p className="my-2">Hubungi via Whatsapp</p>
                  <span className="mt-[1.5vh]">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M1.3335 14.6668L4.95329 13.5127C5.92551 14.0467 7.01926 14.3223 8.12169 14.3223C11.7328 14.3223 14.6668 11.411 14.6668 7.8279C14.6668 4.24478 11.7328 1.3335 8.12169 1.3335C4.51058 1.3335 1.57655 4.24478 1.57655 7.8279C1.57655 9.00792 1.89773 10.1707 2.51405 11.1785L1.3335 14.6668ZM8.12169 13.0002C7.06266 13.0002 6.21891 12.5773 5.3335 12.0002L3.3335 12.6668L4.00016 11.0002C3.32308 10.0785 3.00016 8.96832 3.00016 7.83137C3.00016 4.85118 5.11822 2.66683 8.12169 2.66683C11.1252 2.66683 13.3335 4.84771 13.3335 7.8279C13.3335 10.8081 11.1252 13.0002 8.12169 13.0002ZM9.87516 11.0837C10.37 11.0837 11.1772 10.7736 11.3769 10.2913C11.455 10.0932 11.5245 9.85202 11.5245 9.63668C11.5245 9.60223 11.5245 9.55055 11.5071 9.50748C11.4463 9.40413 10.0314 8.672 9.88384 8.672C9.79357 8.672 9.66996 8.80568 9.53745 8.96609L9.33509 9.21339C9.19982 9.37381 9.06787 9.50748 8.9637 9.50748C8.89426 9.50748 8.8335 9.48164 8.77273 9.44719C8.28662 9.20602 7.86127 8.95624 7.46197 8.57725C7.13211 8.26717 6.76752 7.80206 6.57655 7.39723C6.55051 7.35417 6.53315 7.3111 6.53315 7.26804C6.53315 7.06993 7.13211 6.69956 7.13211 6.37226C7.13211 6.28612 6.6894 5.11472 6.62864 4.96829C6.54183 4.74435 6.49843 4.67544 6.26405 4.67544C6.1512 4.67544 6.04704 4.6496 5.94287 4.6496C5.76058 4.6496 5.62169 4.71851 5.49148 4.83909C5.07482 5.22669 4.86648 5.63152 4.84912 6.19999V6.2689C4.84044 6.86321 5.13558 7.45753 5.46544 7.93987C6.21197 9.03375 6.98454 9.98983 8.22586 10.5497C8.59912 10.722 9.4585 11.0837 9.87516 11.0837Z"
                        fill="white"
                      />
                    </svg>
                  </span>{" "}
                </div>
              </button>
            </Modal>
            {/* End Modal Button Terima */}

            {/* Modal Button Status */}
            <Modal visible={isModalStatusVisible} onOk={handleOkStatus} onCancel={handleCancelStatus} width={360} footer={null}>
              <p className="text-body-14 font-semibold mt-6">Perbarui status penjualan produkmu</p>
              <form onSubmit={handleStatusSubmit}>
                <input type="radio" id="success" name="status_penjualan" value="berhasil-terjual" onChange={handleChecked} className="appearance-none checked:bg-primary-darkblue04 cursor-pointer" />
                <label htmlFor="success" className="text-body-14 font-medium inline-block ml-4">
                  Berhasil terjual
                </label>
                <p className="text-body-14 text-neutral03 ml-8 inline-block mt-2">Kamu telah sepakat menjual produk ini kepada pembeli</p>
                <br />
                <input type="radio" id="cancel" name="status_penjualan" value="batal-terjual" onChange={handleChecked} className="appearance-none checked:bg-primary-darkblue04 cursor-pointer" />
                <label htmlFor="cancel" className="text-body-14 font-medium inline-block ml-4">
                  Batalkan transaksi
                </label>
                <p className="text-body-14 text-neutral03 ml-8 inline-block mt-2 mb-6">Kamu membatalkan transaksi produk ini dengan pembeli</p>
                {isChecked ? (
                  <button type="submit" className=" rounded-2xl w-full text-white text-body-14 bg-primary-darkblue04 py-1">
                    <p className="my-2">Kirim</p>
                  </button>
                ) : (
                  <button type="submit" className=" rounded-2xl w-full text-white text-body-14 bg-neutral03 py-1" disabled={true}>
                    <p className="my-2">Kirim</p>
                  </button>
                )}
              </form>
            </Modal>
          </div>
          {/* Modal Button Tolak */}
          <Modal title={""} visible={isModalRejectedVisible} onOk={handleOkRejected} onCancel={handleCancelRejected}>
            <p>Apakah anda yakin ingin menolak ?</p>
          </Modal>
          {/* End Modal Button Tolak */}
        </div>
      )}
    </>
  );
}

export default InfoPenawarPage;
