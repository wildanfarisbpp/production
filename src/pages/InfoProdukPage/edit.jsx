import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Form, Upload, Modal, Select, Alert, Empty } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import "../../App.css";

import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { getProductById, editProductById, getCategory } from "../../stores/actions/product";

const getBase64 = (file) => // function mengubah file ke base64 untuk menampilkan preview gambar
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

function EditProdukPage() {
  const { id } = useParams()

  const dispatch = useDispatch()
  const { 
    getDetailProductResult, getDetailProductLoading, getDetailProductError, 
    getEditProductResult, getEditProductLoading, getEditProductError,
    getCategoryResult
  } = useSelector((state) => state.productReducer);

  const { Option } = Select;
  const children = [];
  for (let i = 0; i < getCategoryResult.length; i++) {
    children.push(<Option key={getCategoryResult[i].id}>{getCategoryResult[i].name}</Option>);
  }

  // function mengupdate state ukuran layar apakah desktop atau mobile
  const [isDesktop, setDesktop] = useState(window.innerWidth >= 640);
  const updateMedia = () => { setDesktop(window.innerWidth >= 640); };

  // untuk memunculkan alert
  const [showAlert, setShowAlert] = useState(false);

  // jika sudah ada hasil dari getDetailProduct maka akan ditampilkan kembali dalam form
  let initialValues, listInitialImages = []
  if (!getDetailProductResult) {
    initialValues = null;
    listInitialImages = null;
  } else {
    initialValues = {
      productName: getDetailProductResult.productName,
      price: getDetailProductResult.price,
      category: getDetailProductResult.category,
      description: getDetailProductResult.description
    } 
    if (getDetailProductResult.image1 !== null) {
      listInitialImages.push(
          {
            uid: `product_image_1`,
            name: `product_image_1.png`,
            status: 'done',
            url: getDetailProductResult.image1,
          },
      )
    }
    if (getDetailProductResult.image2 !== null) {
      listInitialImages.push(
        {
          uid: `product_image_2`,
          name: `product_image_2.png`,
          status: 'done',
          url: getDetailProductResult.image2,
        },
      )
    }
    if (getDetailProductResult.image3 !== null) {
      listInitialImages.push(
        {
          uid: `product_image_3`,
          name: `product_image_3.png`,
          status: 'done',
          url: getDetailProductResult.image3,
        },
      )
    }
    if (getDetailProductResult.image4 !== null) {
      listInitialImages.push(
        {
          uid: `product_image_4`,
          name: `product_image_4.png`,
          status: 'done',
          url: getDetailProductResult.image4,
        },
      )
    }
  }

  // untuk menyimpan token user
  let accessToken
  if (JSON.parse(localStorage.getItem('userLocal')) !== null) {
    accessToken = JSON.parse(localStorage.getItem('userLocal')).accessToken
  } else {
    accessToken = null
  }

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);
  const [formEditProduk] = Form.useForm();

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf("/") + 1));
  };
  const handleChange = ({ fileList: newFileList }) => setFileList(newFileList);
  const handleCancel = () => setPreviewVisible(false);

  const handleSubmitEdit = async (values) => {
    let inputEditProduct;
    if (values.imageProduk.url) { // jika gambar sudah dalam bentuk link = sudah diupload sebelumnya
      inputEditProduct = {
        productName: values.productName,
        price: values.price,
        category: values.category,
        description: values.description
        // imageProduk tidak diubah
      }
    } else {
      inputEditProduct = {
        productName: values.productName,
        price: values.price,
        category: values.category,
        description: values.description,
        image: values.imageProduk
      }
    }
    console.log(inputEditProduct)
    dispatch(editProductById({id: id, values: inputEditProduct, token: accessToken}))
    formEditProduk.resetFields()
  };

  // jika sudah selesai loading & dapat result
  if (getEditProductResult) {
    localStorage.setItem('setAlertSuccessProduct', true)
    window.location.replace('/daftar-jual/semua')
  } else if (getEditProductError) {
    localStorage.setItem('setAlertErrorProduct', true)
    window.location.reload()
  }

  useEffect(() => {
    dispatch(getProductById({id: id, token: accessToken})); // untuk get info produk yang sudah ada
    dispatch(getCategory(accessToken)) // mengambil daftar kategori

    // untuk menampilkan preview gambar user hasil get
    if (JSON.parse(localStorage.getItem('productImage4'))) {
      setFileList([
        {
          uid: 'product_image1', name: 'product_image1.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage1')),
        },
        {
          uid: 'product_image2', name: 'product_image2.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage2')),
        },
        {
          uid: 'product_image3', name: 'product_image3.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage3')),
        },
        {
          uid: 'product_image4', name: 'product_image4.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage4')),
        }
      ])
    } else if (JSON.parse(localStorage.getItem('productImage3'))) {
      setFileList([
        {
          uid: 'product_image1', name: 'product_image1.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage1')),
        },
        {
          uid: 'product_image2', name: 'product_image2.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage2')),
        },
        {
          uid: 'product_image3', name: 'product_image3.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage3')),
        }
      ])
    } else if (JSON.parse(localStorage.getItem('productImage2'))) {
      setFileList([
        {
          uid: 'product_image1', name: 'product_image1.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage1')),
        },
        {
          uid: 'product_image2', name: 'product_image2.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage2')),
        }
      ])
    } else if (JSON.parse(localStorage.getItem('productImage1'))) {
      setFileList([
        {
          uid: 'product_image1', name: 'product_image1.png', status: 'done',
          url: JSON.parse(localStorage.getItem('productImage1')),
        }
      ])
    }

    // untuk menampilkan alert setelah edit produk
    if (JSON.parse(localStorage.getItem('setAlertErrorProduct')) === true) {
        setShowAlert("Error")
        localStorage.setItem('setAlertErrorProduct', false)
    }

    // untuk mengupdate state ukuran layar apakah desktop atau mobile
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);

    }, [dispatch, id, accessToken]);

  return (
    <>
    { getEditProductLoading && <LoadingWithBackground /> }
    { showAlert === "Error" && <Alert message={"Produk gagal diedit !"} type="error" closable className="custom-alert" /> }
    { getDetailProductLoading 
    ? (<LoadingWithBackground />)
    : ( getDetailProductResult 
        ? <div className="flex flex-col gap-6 sm:flex-row w-full justify-center px-4 pb-6 sm:px-0 sm:py-10 relative">
        {/* start tampilan atas page (tanda back) */}
        {isDesktop ? (
            <Link to={`/produk-seller/${id}`}>
            <img src="/assets/images/fi_arrow-left.svg" alt="<" className="absolute sm:left-12 lg:left-52" />
            </Link>
        ) : (
            <div className="py-4 flex flex-row items-center gap-4 relative">
            <Link to={`/produk-seller/${id}`} className="absolute left-0">
                <img src="/assets/images/fi_arrow-left.svg" alt="<" />
            </Link>
            <span className="grow text-center font-medium text-sm">Lengkapi Detail Produk</span>
            </div>
        )}
        {/* end tampilan atas page (tanda back) */}
        
        {/* start tampilan form */}
        <Form layout="vertical" 
            form={formEditProduk} 
            onFinish={handleSubmitEdit} 
            className="w-full sm:w-2/3 lg:w-2/5"
            initialValues={initialValues}
        >
            <Form.Item
            label="Nama Produk"
            name="productName"
            rules={[
                {
                required: true,
                message: "Harap isi nama produk !",
                },
            ]}
            >
            <input id="productName" name="namaProduk" placeholder="Nama Produk" className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none" />
            </Form.Item>
            <Form.Item
            label="Harga Produk"
            name="price"
            rules={[
                {
                required: true,
                message: "Harap isi harga produk !",
                },
            ]}
            >
            <input type="number" id="price" name="hargaProduk" placeholder="Rp 0,00" className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none" />
            </Form.Item>
            <Form.Item
            name="category"
            label="Kategori"
            rules={[
                {
                required: true,
                message: "Harap pilih kategori !",
                },
            ]}
            >
            <Select
                allowClear
                showArrow
                suffixIcon={<img src="/assets/images/fi_chevron-down.svg" alt="v" />}
                style={{
                width: "100%",
                }}
                placeholder="Pilih Kategori"
                disabled
            >
                {children}
            </Select>
            </Form.Item>
            <Form.Item
            label="Deskripsi"
            name="description"
            rules={[
                {
                required: true,
                message: "Harap isi deskripsi !",
                },
            ]}
            >
            <textarea id="description" maxLength={100} name="alamatUser" placeholder="Contoh: Jalan Ikan Hiu 33" className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full resize-none hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none"></textarea>
            </Form.Item>
            <Form.Item
            label="Foto Produk"
            name="imageProduk"
            getValueFromEvent={(e) => {
                return e.fileList;
            }}
            initialValue={listInitialImages}
            rules={[
                {
                required: true,
                message: "Harap masukkan foto !",
                },
            ]}
            extra="Foto yang diupload max. 1 MB"
            >
            <Upload id="imageProduk" listType="picture-card" fileList={fileList} onPreview={handlePreview} onChange={handleChange} maxCount={4} beforeUpload={() => false}>
                { fileList.length >= 4 
                ? null 
                : (
                <PlusOutlined
                style={{
                    fontSize: 24,
                    color: "#8A8A8A",
                    backgroundColor: "#FFFFFF",
                    width: "100%",
                    height: "100%",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: "12px",
                    border: "1px dashed #D0D0D0",
                }}
                /> )}
            </Upload>
            </Form.Item>
            <div className="flex flex-row gap-4 w-full">
            <Link to={`/produk-seller/${id}`} className="w-full">
                <button className="text-body-14 leading-5 border border-primary-darkblue04 text-neutral05 font-medium px-6 py-3 rounded-2xl w-full">
                Batal
                </button>
            </Link>
            <button key="submit" htmlType="submit" className="text-body-14 leading-5 bg-primary-darkblue04 hover:bg-primary-darkblue05 text-neutral01 font-medium px-6 py-3 rounded-2xl w-full">
                Submit
            </button>
            </div>
        </Form>
        {/* end tampilan form */}

        {/* start modal untuk preview foto */}
        <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={handleCancel}>
            <img alt="Gambar Profile" style={{ width: "100%" }} src={previewImage} />
        </Modal>
        {/* end modal untuk preview foto */}
        </div>
        : <div className="w-screen h-screen flex justify-center items-center">
            <Empty/>
        </div>
    ) }
    </>
  );
}

export default EditProdukPage;
