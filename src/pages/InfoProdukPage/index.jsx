import React, { useState, useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Form, Upload, Modal, Select, Alert } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import "../../App.css";

import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, getCategory } from "../../stores/actions/product";
import { getUserProfile } from "../../stores/actions/profile";

const getBase64 = (
  file // function mengubah file ke base64 untuk menampilkan preview gambar
) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

function InfoProdukPage() {
  const location = useLocation();
  console.log('State (in form) :', location.state)

  const [submitFormStatus, setSubmitFormStatus] = useState(null);

  const dispatch = useDispatch()
  const { 
    getAddProductResult, getAddProductLoading, getAddProductError,
    getCategoryResult
  } = useSelector((state) => state.productReducer);
  const { 
    getDetailProfileResult, getDetailProfileLoading 
  } = useSelector((state) => state.profileReducer);


  const { Option } = Select;
  const children = [];
  for (let i = 0; i < getCategoryResult.length; i++) {
    children.push(<Option key={getCategoryResult[i].id}>{getCategoryResult[i].name}</Option>);
  }

  // function mengupdate state ukuran layar apakah desktop atau mobile
  const [isDesktop, setDesktop] = useState(window.innerWidth >= 640);
  const updateMedia = () => {
    setDesktop(window.innerWidth >= 640);
  };

  // untuk memunculkan alert
  const [showAlert, setShowAlert] = useState(false);

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);
  const [formInfoProduk] = Form.useForm();

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf("/") + 1));
  };
  const handleChange = ({ fileList: newFileList }) => setFileList(newFileList);
  const handleCancel = () => setPreviewVisible(false);

  // untuk menyimpan state preview produk
  const [inputProduk, setInputProduk] = useState({
    productName: '',
    price: '',
    category: [],
    description: ''
  });

  const handleProdukInput = e => {
      setInputProduk({
          ...inputProduk,
          [e.target.id]: e.target.value
      });
  };

  const handleProdukCategoryInput = (value, event) => {
    console.log(event)
    setInputProduk({
      ...inputProduk,
      category: event
  });
  }

  const navigate = useNavigate();
  const toPreview = (valuesPreview, imagePreview) => {
    navigate('/produk', { state: {  
      name:'Preview',
      productName: valuesPreview.productName, 
      price: valuesPreview.price,
      categoryId: valuesPreview.category.value,
      categoryName: valuesPreview.category.children,
      description: valuesPreview.description,
      imageProduk: imagePreview
    } });
  }

  const handleSubmit = async (values) => {
    console.log(values)
    let inputAddProduct;
    if (values.imageProduk.url) {
      // jika gambar sudah dalam bentuk link = sudah diupload sebelumnya
      inputAddProduct = {
        productName: values.productName,
        price: values.price,
        category: values.category,
        description: values.description,
        // imageProduk tidak diubah
      };
    } else {
      inputAddProduct = {
        productName: values.productName,
        price: values.price,
        category: values.category,
        description: values.description,
        image: values.imageProduk
      }
    }
    dispatch(addProduct({ status: "publish", values: inputAddProduct, token: accessToken }));
    formInfoProduk.resetFields();
  };

  // jika sudah ada data produk yang tersimpan di state (dari preview) maka akan ditampilkan kembali dalam form
  let initialValues, listInitialImages = []
  if (location.state === null) {
    initialValues = null;
    listInitialImages = null;
  } else {
    initialValues = {
      productName: location.state.productName,
      price: location.state.price,
      category: location.state.categoryId,
      description: location.state.description
    }
    for (let i = 0; i < location.state.imageProduk.length; i++) {
      listInitialImages.push(location.state.imageProduk[i])
    }
  }

  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  // jika seller belum melengkapi profilnya, dia harus melengkapinya terlebih dahulu sebelum jual barang
  if (
    getDetailProfileResult.fullname === null || getDetailProfileResult.city === null || 
    getDetailProfileResult.address === null || getDetailProfileResult.phone === null || 
    getDetailProfileResult.imageProfil === null
    ) {
    localStorage.setItem("setAlertLengkapiProfile", true);
    window.location.replace("/info-profil");
  }

  // jika sudah selesai loading & dapat result
  if (getAddProductResult) {
    localStorage.setItem("setAlertSuccessProduct", true);
    window.location.replace("/daftar-jual/semua");
  } else if (getAddProductError) {
    localStorage.setItem("setAlertErrorProduct", true);
    window.location.reload();
  }

  useEffect(() => {
    dispatch(getCategory(accessToken)) // mengambil daftar kategori

    let listImage = []
    if (location.state !== null) {
      for (let i = 0; i < location.state.imageProduk.length; i++) {
        listImage.push(location.state.imageProduk[i])
      }
      setFileList(listImage)
      setInputProduk({
        productName: location.state.productName,
        price: location.state.price,
        category: location.state.categoryId,
        description:  location.state.description
    });
    }

    // untuk menampilkan alert setelah tambah produk
    if (JSON.parse(localStorage.getItem('setAlertErrorProduct')) === true) {
      setShowAlert("Error")
      localStorage.setItem('setAlertErrorProduct', false)
    }

    // untuk mengupdate state ukuran layar apakah desktop atau mobile
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  }, [dispatch, accessToken, location.state]);

  return (
    <>
      {getAddProductLoading && <LoadingWithBackground />}
      {showAlert === "Error" && <Alert message={"Produk gagal diterbitkan !"} type="error" closable className="custom-alert" />}
      <div className="flex flex-col gap-6 sm:flex-row w-full justify-center px-4 pb-6 sm:px-0 sm:py-10 relative">
        {/* start tampilan atas page (tanda back) */}
        {isDesktop ? (
        <Link to="/daftar-jual/semua">
          <img src="/assets/images/fi_arrow-left.svg" alt="<" className="absolute sm:left-12 lg:left-52" />
        </Link>
      ) : (
          <div className="py-4 flex flex-row items-center gap-4 relative">
            <Link to="/daftar-jual/semua" className="absolute left-0">
              <img src="/assets/images/fi_arrow-left.svg" alt="<" />
            </Link>
            <span className="grow text-center font-medium text-sm">Lengkapi Detail Produk</span>
          </div>
      )
      }
      {/* end tampilan atas page (tanda back) */}
      
      {/* start tampilan form */}
      <Form layout="vertical" 
        form={formInfoProduk} 
        onFinish={handleSubmit} 
        className="w-full sm:w-2/3 lg:w-2/5"
        initialValues={initialValues}
      >
        <Form.Item
          label="Nama Produk"
          name="productName"
          rules={[
            {
              required: true,
              message: "Harap isi nama produk !",
            },
          ]}
        >
          <input id="productName" name="namaProduk" placeholder="Nama Produk" onChange={handleProdukInput} className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none" />
        </Form.Item>
        <Form.Item
          label="Harga Produk"
          name="price"
          rules={[
            {
              required: true,
              message: "Harap isi harga produk !",
            },
          ]}
        >
          <input type="number" id="price" name="hargaProduk" placeholder="Rp 0,00" onChange={handleProdukInput} className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none" />
        </Form.Item>
        <Form.Item
          name="category"
          label="Kategori"
          rules={[
            {
              required: true,
              message: "Harap pilih kategori !",
            },
          ]}
        >
          <Select
            // mode="multiple"
            allowClear
            showArrow
            suffixIcon={<img src="/assets/images/fi_chevron-down.svg" alt="v" />}
            style={{
              width: "100%",
            }}
            placeholder="Pilih Kategori"
            onSelect={(value, event) => handleProdukCategoryInput(value, event)}
            // onChange={(value) => {
            //   if (value?.length > 5) {
            //     value.pop();
            //   }
            // }}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item
          label="Deskripsi"
          name="description"
          rules={[
            {
              required: true,
              message: "Harap isi deskripsi !",
            },
          ]}
        >
          <textarea id="description" maxLength={300} name="alamatUser" onChange={handleProdukInput} placeholder="Contoh: Jalan Ikan Hiu 33" className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full resize-none hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none"></textarea>
        </Form.Item>
        <Form.Item
          label="Foto Produk"
          name="imageProduk"
          getValueFromEvent={(e) => {
            return e.fileList;
          }}
          initialValue={listInitialImages}
          rules={[
            {
              required: true,
              message: "Harap masukkan foto !",
            },
          ]}
          extra="Foto yang diupload max. 1 MB"
        >
          <Upload id="imageProduk" listType="picture-card" fileList={fileList} onPreview={handlePreview} onChange={handleChange} maxCount={4} beforeUpload={() => false}>
            { fileList.length >= 4 
            ? null 
            : (
            <PlusOutlined
              style={{
                width: "100%",
              }}
            /> )}
          </Upload>
        </Form.Item>
        <div className="flex flex-row gap-4 w-full">
          <button key="goToPreview" onClick={()=>{ toPreview(inputProduk, fileList) }} className="text-body-14 leading-5 border border-primary-darkblue04 text-neutral05 font-medium px-6 py-3 rounded-2xl w-full">
            Preview
          </button>
          <button key="submitPublish" htmlType="submit" onClick={() => setSubmitFormStatus('publish')} className="text-body-14 leading-5 bg-primary-darkblue04 hover:bg-primary-darkblue05 text-neutral01 font-medium px-6 py-3 rounded-2xl w-full">
            Terbitkan
          </button>
        </div>
      </Form>
      {/* end tampilan form */}

        {/* start modal untuk preview foto */}
        <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={handleCancel}>
          <img alt="Gambar Profile" style={{ width: "100%" }} src={previewImage} />
        </Modal>
        {/* end modal untuk preview foto */}
      </div>
    </>
  );
}

export default InfoProdukPage;
