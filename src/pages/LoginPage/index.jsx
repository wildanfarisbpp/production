import React, { useState } from "react";

import { Alert, Button, Form, Input } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector} from "react-redux";
import { getLogin } from "../../stores/actions/auth";

function LoginPage() {
  const { getLoginLoading, getLoginError, getLoginErrorMessage, getLoginSuccess} = useSelector(state => state.authReducer)
  const dispatch = useDispatch()
  const [input, setInput] = useState({
    email:'',
    password:''
  })
  
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  

  const handleInput = (e) => {
    setInput({
      ...input, 
      [e.target.id]: e.target.value
    })
  }

  const handleLogIn = async() => {
    dispatch(getLogin(input))
    JSON.parse(localStorage.getItem("userLocal"))
  }

  return (
    <>
    <div className="grid grid-cols-1 lg:grid-cols-2" id="loginpage">
        <div>
          <img src="/assets/images/bg-login.png" alt="bg" className="hidden lg:block h-screen w-full object-cover"></img>
        </div>
        <div className="flex flex-col h-screen justify-unset lg:justify-center pt-[115px] lg:pt-0">
          <div className="flex items-center justify-unset sm:justify-center relative bottom-[40px] my-2 lg:my-0">
            {getLoginError && <Alert type="error" message={getLoginErrorMessage} className="!bg-[#fff2f0] !rounded-xl !w-11/12 md:!w-2/3"/>}
            {getLoginSuccess && <Alert type="success" message="Login Successful!" className="!rounded-xl !w-11/12 md:!w-2/3"/> }
          </div>
          <Link to={"/"} className="text-[#000000] block lg:!hidden absolute top-[10px]">
            <img src="/assets/images/fi_arrow-left.svg" alt="left" className="pt-5 pl-3 !text-left" />
          </Link>
          <div className="w-11/12 md:w-2/3 mx-auto">
            <p className="text-title-24 font-bold">Masuk</p>
            <Form
              name="basic"
              layout="vertical"
              onFinish={handleLogIn}
              onFinishFailed={onFinishFailed}
              autoComplete="off">
              <Form.Item
                label={<div className="text-body-12">Email</div>}
                name="email"
                style={{ marginBottom: "16px" }}
                rules={[
                  {
                    required: true,
                    message: "Please input Email!",
                  },
                  {
                    type: 'email',
                    message: "Invalid Email"
                  }
                ]}
              >
                <Input
                  placeholder="Contoh: johndee@gmail.com"
                  className="!rounded-2xl !py-3 !px-4"
                  id="email"
                  onChange={handleInput} />
              </Form.Item>

              <Form.Item
                label={<div className="text-body-12">Password</div>}
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input Password!",
                  },
                ]}
              >
                <Input.Password
                  placeholder="Masukkan password"
                  className="!rounded-2xl !py-3 !px-4"
                  id="password"
                  onChange={handleInput} />
              </Form.Item>
              <Form.Item>
                  {getLoginLoading ? (
                    <Button htmlType="submit" className="!flex !items-center !justify-center !rounded-xl !py-5 !bg-primary-darkblue04 !text-neutral01 !w-full" loading>
                      Masuk
                    </Button>
                  ) : (
                    <Button htmlType="submit" className="!flex !items-center !justify-center !rounded-xl !py-5 !bg-primary-darkblue04 !text-neutral01 !w-full">
                      Masuk
                    </Button>
                  )
                }
              </Form.Item>
              <Form.Item className="absolute lg:relative bottom-[10px] lg:top-0 left-1/2 lg:left-0 -translate-x-1/2 lg:-translate-x-0 w-full">
                <div className="mt-3 text-center">
                  Belum punya akun?
                  <a href="/register" className="text-primary-darkblue04 font-bold">
                    {" "}
                    Daftar di sini
                  </a>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
        
      </>
    
    );
}

export default LoginPage;