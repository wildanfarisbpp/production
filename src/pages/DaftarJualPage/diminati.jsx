import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { productWishlist } from "../../stores/actions/history";
import { Link } from "react-router-dom";
import LoadingWithBackground from "../../Components/Loading/withBackground";
import moment from "moment";
import { Skeleton } from "antd";

function ProdukDiminati() {
  const { getProductWishlistResult, getProductWishlistLoading } = useSelector((state) => state.historyReducer);
  const dispatch = useDispatch();
  const getDate = (date) => {
    return date;
  };

  useEffect(() => {
    dispatch(productWishlist({ token: JSON.parse(localStorage.getItem("userLocal")).accessToken }));
  }, [dispatch]);
  return (
    <>
      {getProductWishlistLoading && (
        <>
          <Skeleton active loading={true} style={{ width: "300px" }} />
        </>
      )}
      <div className="grow h-min flex flex-col items-start w-full sm:w-auto pb-6">
        {getProductWishlistResult.length === 0 && (
          <div className="max-w-[280px] flex flex-col mx-auto justify-center items-center gap-6">
            <img src="/assets/images/Group 33.svg" alt="No Product" />
            <span className="text-sm font-normal text-center">Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok</span>
          </div>
        )}

        {getProductWishlistResult &&
          getProductWishlistResult.map((product) => {
            return (
              <Link to={`/info-penawar/${product.offerId}`} className="w-full">
                <div className="bg-neutral01 flex flex-row flex-wrap gap-4 pb-6 w-full">
                  <img src={product.image1} alt="Profile User" className="w-12 h-12 object-cover rounded-xl" />
                  <div className="flex flex-col gap-1">
                    <span className="text-body-10 leading-3 font-normal text-neutral03">Penawaran produk</span>
                    <span className="text-sm font-normal text-neutral05 max-w-[160px] md:max-w-none truncate">{product.productName}</span>
                    <span className="text-sm font-normal text-neutral05">
                      Rp{" "}
                      {parseInt(product.price)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                        .toString()}
                    </span>
                    <span className="text-sm font-normal text-neutral05">
                      Ditawar Rp{" "}
                      {product.priceNegotiated
                        ? parseInt(product.priceNegotiated)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()
                        : "Penawar gk masukin harga nih!!"}
                    </span>
                  </div>
                  <span className="text-body-10 leading-3 font-normal text-neutral03 ml-auto mr-0">{moment(product.createdAt).format("DD MMMM h:mm")}</span>
                </div>
              </Link>
            );
          })}
      </div>
    </>
  );
}

export default ProdukDiminati;
