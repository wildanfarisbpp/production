import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { productSeller } from "../../stores/actions/history";
import CardProduct from "../../Components/Card/Card";
import { Skeleton } from "antd";

function ProdukSemua() {
  const dispatch = useDispatch();
  const { getProductSellerResult, getProductSellerLoading } = useSelector((state) => state.historyReducer);

  useEffect(() => {
    dispatch(productSeller({ token: JSON.parse(localStorage.getItem("userLocal")).accessToken }));
  }, [dispatch]);

  return (
    <div className="grow h-min gap-6 grid grid-cols-2 lg:grid-cols-3">
      <Link to="/info-produk">
        <div className="w-auto h-full min-h-[200px] pt-2 px-2 pb-4 flex flex-col flex-wrap gap-3 justify-center items-center border border-dashed border-neutral02 rounded-md">
          <img src="/assets/images/fi_plus.svg" alt="+" />
          <span className="font-normal text-xs text-neutral03 text-center">Tambah Produk</span>
        </div>
      </Link>
      {getProductSellerLoading && (
        <>
          <div class="flex flex-grow-0">
            <Skeleton active loading={true} />
          </div>
          <div class="flex flex-grow-1">
            <Skeleton active loading={true} />
          </div>
          <div class="flex flex-grow-2">
            <Skeleton active loading={true} />
          </div>
        </>
      )}

      {getProductSellerResult && <CardProduct product={getProductSellerResult} />}
    </div>
  );
}

export default ProdukSemua;
