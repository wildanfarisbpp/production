import React, { useState } from "react";

import { Link } from "react-router-dom";
import { Alert, Button, Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getRegister } from "../../stores/actions/auth";

function RegisterPage() {
  const { getRegistLoading, getRegistError, getRegistErrorMessage, getRegistSuccess } = useSelector(state => state.authReducer)
  const dispatch = useDispatch()
  const [input, setInput] = useState({
    fullname: '',
    username:'',
    email:'',
    password:''
  })

  const handleInput = e => {
    setInput({
      ...input, 
      [e.target.id]: e.target.value
    })
  }

  const onFinish = async () => {
    dispatch(getRegister(input)) 
    JSON.parse(localStorage.getItem("userLocal"))   
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="grid grid-cols-1 lg:grid-cols-2" id="registerpage">
      <div>
        <img src="/assets/images/bg-login.png" alt="bg" className="hidden lg:block object-cover w-full h-screen"></img>
      </div>
      <div className="flex flex-col h-screen justify-unset lg:justify-center pt-[110px] lg:pt-4 w-11/12 md:w-4/5 lg:w-full mx-auto">
        <div className="flex items-center justify-unset sm:justify-center relative bottom-[44px] sm:bottom-[30px] lg:bottom-[23px]">
            {getRegistError && <Alert type="error" message={getRegistErrorMessage} className="!bg-[#fff2f0] !rounded-xl md:!w-2/3"/>}
            {getRegistSuccess && <Alert type="success" message="Registration Successful!" className="!rounded-xl !w-11/12 md:!w-2/3"/> }
          </div>
        <Link to={"/"} className="text-[#000000] block lg:!hidden absolute top-[10px]">
          <img src="/assets/images/fi_arrow-left.svg" alt="left" className="pt-5 pl-3 !text-left" />
        </Link>
        <div className="w-11/12 lg:w-2/3 mx-auto">
          <p className="text-title-24 font-bold mb-2">Daftar</p>
          <Form name="basic" layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
            <Form.Item
              label={<div className="text-body-12">Nama</div>}
              name="fullname"
              style={{ marginBottom: "10px" }}
              rules={[
                {
                  required: true,
                  message: "Please input fullname!",
                },
              ]}
            >
              <Input 
                placeholder="Nama Lengkap" 
                className="!rounded-2xl !py-3 !px-4"
                id="fullname"
                onChange={handleInput} 
              />
            </Form.Item>
            <Form.Item
              label={<div className="text-body-12">Username</div>}
              name="username"
              style={{ marginBottom: "10px" }}
              rules={[
                {
                  required: true,
                  message: "Please input Username!",
                },
                {
                  pattern: new RegExp("^[a-zA-Z0-9]{6,}$"),
                  message: "Username must be at least 6 characters"
                }
              ]}
            >
              <Input 
                placeholder="Contoh: johndee" 
                className="!rounded-2xl !py-3 !px-4"
                id="username"
                onChange={handleInput} 
              />
            </Form.Item>
            <Form.Item
              label={<div className="text-body-12">Email</div>}
              name="email"
              style={{ marginBottom: "10px" }}
              rules={[
                {
                  required: true,
                  message: "Please input Email!",
                },
                {
                  type: "email",
                  message: "Invalid Email"

                }
              ]}
            >
              <Input 
                placeholder="Contoh: johndee@gmail.com" 
                className="!rounded-2xl !py-3 !px-4" 
                id="email"
                onChange={handleInput}
              />
            </Form.Item>

            <Form.Item
              label={<div className="text-body-12">Password</div>}
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input Password!",
                },
                {
                  // eslint-disable-next-line no-useless-escape
                  pattern: new RegExp("^(?=.*?[0-9]).{8,}$"),
                  message: "Password must be at least 8 characters with a mixture of letters & numbers"
                }
              ]}
            >
              <Input.Password 
                placeholder="Masukkan password" 
                className="!rounded-2xl !py-3 !px-4"
                id="password"
                onChange={handleInput} 
              />
            </Form.Item>
            <Form.Item className="!mb-0">
              {getRegistLoading ? (
                <Button htmlType="submit" className="!flex !items-center !justify-center !rounded-xl !py-5 !bg-primary-darkblue04 !text-neutral01 !w-full" loading>
                  Daftar
                </Button>
                ) : (
                <Button htmlType="submit" className="!flex !items-center !justify-center !rounded-xl !py-5 !bg-primary-darkblue04 !text-neutral01 !w-full">
                  Daftar
                </Button>
                )
              }
            </Form.Item>
            <Form.Item className="absolute lg:relative bottom-[10px] lg:top-0 left-1/2 lg:left-0 -translate-x-1/2 lg:-translate-x-0 w-full">
              <div className="mt-3 text-center">
                Sudah punya akun?
                <a href="/login" className="text-primary-darkblue04 font-bold">
                  {" "}
                  Masuk di sini
                </a>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
