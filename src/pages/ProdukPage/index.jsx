import React, { Component, useEffect } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Navbar from "../../Components/Navbar";
import { useLocation, useNavigate } from 'react-router-dom';
import { Empty } from "antd";
import { useMediaQuery } from "react-responsive";

import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { addProduct } from "../../stores/actions/product";
import { getUserProfile } from "../../stores/actions/profile";

// menampilkan gambar
const loadFile = async (fileImg) => {
  for(let i=1; i<= fileImg.length; i++) {
    const preview = document.getElementById(`output${i}`);
    const file = fileImg[i-1].originFileObj;

    const reader = new FileReader();
    reader.addEventListener("load", function () {
      preview.src = reader.result; // convert image file to base64 string
    }, false);

    if (file) { reader.readAsDataURL(file); }
  }
};

export class Fade extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    // loadFile(props.data)
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div className="relative overflow-hidden md:rounded-2xl">
        <Slider ref={(c) => (this.slider = c)} {...settings}>
          <div>
            <img id="output1" src='https://via.placeholder.com/400x300?text=No+Image' className="w-full" alt="foto produk 1" />
          </div>
          { this.props.data[1] &&
          <div>
            <img id="output2" src="" className="w-full" alt="foto produk 2" />
          </div>
          }
          { this.props.data[2] &&
          <div>
            <img id="output3" src="" className="w-full" alt="foto produk 3" />
          </div>
          }
          { this.props.data[3] &&
          <div>
            <img id="output4" src="" className="w-full" alt="foto produk 4" />
          </div>
          }
        </Slider>
        <div className="btn-carousel hidden md:block absolute left-3 top-[45%] ">
          <button className=" rounded-full " onClick={this.previous}>
            <img className="rotate-180" src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
        <div className="btn-carousel hidden md:block absolute right-3 top-[45%] ">
          <button className=" rounded-full " onClick={this.next}>
            <img src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
      </div>
    );
  }
}

function ProdukPage() {
  const location = useLocation();
  console.log('State (in preview) :', location.state)

  const navigate = useNavigate();
  const backToForm = (valuesPreview) => {
    navigate('/info-produk', { state: {  
      name:'Back To Form',
      productName: valuesPreview.productName, 
      price: valuesPreview.price,
      categoryId: valuesPreview.categoryId,
      description: valuesPreview.description,
      imageProduk: valuesPreview.imageProduk
    } });
  }

  // addProduct
  const dispatch = useDispatch()
  const { 
    getAddProductResult, getAddProductLoading, getAddProductError,
  } = useSelector((state) => state.productReducer);
  const { 
    getDetailProfileResult, getDetailProfileLoading 
  } = useSelector((state) => state.profileReducer);

  // untuk menyimpan token user
  let accessToken
  if (JSON.parse(localStorage.getItem('userLocal')) !== null) {
    accessToken = JSON.parse(localStorage.getItem('userLocal')).accessToken
  } else {
    accessToken = null
  }

  // jika sudah selesai loading & dapat result
  if (getAddProductResult) {
    localStorage.setItem('setAlertSuccessProduct', true)
    window.location.replace('/daftar-jual/semua')
  } else if (getAddProductError) { // belum ada set
    localStorage.setItem('setAlertErrorProduct', true)
    window.location.replace('/info-produk')
  }

  const handleSubmitPreview = async (values) => {
    let inputAddProduct;
    if (values.imageProduk.url) { // jika gambar sudah dalam bentuk link = sudah diupload sebelumnya
      inputAddProduct = {
        productName: values.productName,
        price: values.price,
        category: values.categoryId,
        description: values.description
        // imageProduk tidak diubah
      }
    } else {
      inputAddProduct = {
        productName: values.productName,
        price: values.price,
        category: values.categoryId,
        description: values.description,
        image: values.imageProduk
      }
    }
    console.log('Submit:', inputAddProduct)
    dispatch(addProduct({status: "publish", values: inputAddProduct, token: accessToken}))
  };

  useEffect(() => {
    if (location.state !== null) { loadFile(location.state.imageProduk) }
    dispatch(getUserProfile(accessToken)) // mengambil detail user profile
  }, [location.state, accessToken, dispatch]);

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1024px)",
  });
  
  return (
    <>
      { getAddProductLoading && <LoadingWithBackground /> }
      {isDesktopOrLaptop && <Navbar />}
      {!isDesktopOrLaptop && 
        <button onClick={()=> backToForm(location.state)}>
          <img src="/assets/images/fi_arrow-left-bg-white.svg" className="absolute z-50 mx-4 my-11 cursor-pointer" alt="<"/> 
        </button>
      }
      { location.state
      ?
      <div className="container mx-auto max-w-4xl">
        <div className="grid md:grid-cols-5 grid-cols-1 gap-6">
          <div className="col-span-3">
            <Fade data={location.state.imageProduk}/>
            {/* responsive */}
            <div className="container mx-auto max-w-4xl mt-3 -translate-y-12">
              <div className="shadow-auto rounded-lg p-4 bg-white text-base font-medium h-auto mx-4 px-3 md:hidden">
                <h2 className="text-base font-medium break-words">{location.state.productName}</h2>
                <p className="text-sm font-normal text-[#8A8A8A]">{location.state.categoryName}</p>
                <p className="font-normal text-base">Rp {location.state.price}</p>
              </div>
            </div>
            {/* responsive penjual */}
            <div className="container mx-auto max-w-4xl mt-3 -translate-y-11">
              <div className="flex md:hidden shadow-auto rounded-lg bg-white p-4 text-base font-medium  mx-4 px-3">
                <img className="w-12" src={getDetailProfileResult.imageProfile || "/images/user_image.png"} alt="user" />
                <div className="ml-4">
                  <h1 className="font-medium text-sm break-words">{getDetailProfileResult.fullname}</h1>
                  <h1 className="text-sm font-normal text-[#8A8A8A] ">{getDetailProfileResult.city}</h1>
                </div>
              </div>
            </div>

            {/* responsive end */}
            <div className="container mx-auto max-w-4xl mt-5 ">
              <div className="col-span-3 px-4 md:px-0">
                <div className=" shadow-auto rounded-lg bg-white p-4 -translate-y-11 md:-translate-y-0 ">
                  <h2>Deskripsi</h2>
                  <h1 className="py-2 text-[#8A8A8A] break-all">
                    {location.state.description}
                  </h1>
                </div>
              </div>
              <div className="flex justify-center">
                <button onClick={()=> handleSubmitPreview(location.state)} className="md:hidden bg-[#7126B5] w-11/12 py-4 mt-2 rounded-2xl font-medium text-sm text-white fixed bottom-5 ">Terbitkan</button>
              </div>
            </div>
          </div>
          <div className="col-span-2">
            <div className="shadow-auto rounded-lg p-5 md:block hidden">
              <h2 className="text-base font-medium break-words">{location.state.productName}</h2>
              <p className="text-sm font-normal text-[#8A8A8A]">{location.state.categoryName}</p>
              <p className="font-normal text-base">Rp {location.state.price}</p>
              <button onClick={() => handleSubmitPreview(location.state)} className="bg-[#7126B5] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white">Terbitkan</button>
              <button onClick={()=> backToForm(location.state)} className="bg-[#FFFFFF] w-full py-3 mt-2 rounded-2xl font-medium text-sm border-2 border-[#7126B5] transition ease-in-out duration-300">Edit</button>
            </div>
            <div className="col-span-2 mt-5">
              <div className="hidden md:flex shadow-auto rounded-2xl p-4 ">
                <img className="w-12" src={getDetailProfileResult.imageProfile || "/images/user_image.png"} alt="user" />
                <div className="ml-4">
                  <h1 className="font-medium text-sm break-words">{getDetailProfileResult.fullname}</h1>
                  <h1 className="text-sm font-normal text-[#8A8A8A]">{getDetailProfileResult.city}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      : <div className="w-screen h-screen flex justify-center items-center">
        <Empty/>
      </div> 
      }
    </>
  );
}

export default ProdukPage;
