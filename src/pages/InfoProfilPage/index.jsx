import React, { useState, useEffect } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { Form, Upload, Modal, Select, Alert, Empty } from "antd";
import { CameraOutlined } from "@ant-design/icons";
import "../../App.css";

import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { updateUserProfile, getUserProfile } from "../../stores/actions/profile";

const getBase64 = (
  file // function mengubah file ke base64 untuk menampilkan preview gambar
) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

function InfoProfilPage() {
  const dispatch = useDispatch();
  const { getUpdateProfileResult, getUpdateProfileLoading, getUpdateProfileError, getDetailProfileResult, getDetailProfileLoading } = useSelector((state) => state.profileReducer);

  const { Option } = Select;

  const location = useLocation();
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("userLocal");
    localStorage.removeItem("userImage");
    navigate("/login");
  };
  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  // function mengupdate state ukuran layar apakah desktop atau mobile
  const [isDesktop, setDesktop] = useState(window.innerWidth >= 640);
  const updateMedia = () => {
    setDesktop(window.innerWidth >= 640);
  };

  // untuk memunculkan alert
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    if (JSON.parse(localStorage.getItem("setAlertLengkapiProfile")) === true) {
      setShowAlert("Lengkapi-Profile");
      localStorage.setItem("setAlertLengkapiProfile", false);
    }

    dispatch(getUserProfile(accessToken)); // mengambil detail user profile

    // untuk menampilkan preview gambar user hasil get
    if (JSON.parse(localStorage.getItem("userImage")) !== null) {
      setFileList([
        {
          uid: "profile_image",
          name: "profile_image.png",
          status: "done",
          url: JSON.parse(localStorage.getItem("userImage")).url,
        },
      ]);
    }

    // untuk menampilkan alert setelah update profil
    if (JSON.parse(localStorage.getItem("setAlertErrorProfile")) === true) {
      setShowAlert("Error");
      localStorage.setItem("setAlertErrorProfile", false);
    }

    // untuk mengupdate state ukuran layar apakah desktop atau mobile
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  }, [dispatch, accessToken]);

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);
  const [formInfoProfile] = Form.useForm();

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf("/") + 1));
  };
  const handleChange = ({ fileList: newFileList }) => setFileList(newFileList);
  const handleCancel = () => setPreviewVisible(false);

  const handleSubmit = async (values) => {
    let inputEditProfile;
    if (values.imageProfil.url) {
      // jika gambar sudah dalam bentuk link = sudah diupload sebelumnya
      inputEditProfile = {
        address: values.address,
        city: values.city,
        fullname: values.fullname,
        phone: values.phone,
        // imageProfil tidak diubah
      };
    } else {
      inputEditProfile = {
        address: values.address,
        city: values.city,
        fullname: values.fullname,
        phone: values.phone,
        imageProfil: values.imageProfil,
      };
    }
    dispatch(updateUserProfile({ values: inputEditProfile, token: accessToken }));
    formInfoProfile.resetFields();
  };

  // jika sudah ada data user yang tersimpan di localStorage maka akan ditampilkan kembali dalam form
  let initialValues, initialImages;
  if (!getDetailProfileResult) {
    initialValues = null;
    initialImages = null;
  } else {
    initialValues = {
      fullname: getDetailProfileResult.fullname,
      city: getDetailProfileResult.city,
      address: getDetailProfileResult.address,
      phone: getDetailProfileResult.phone,
    };
    if (getDetailProfileResult.imageProfile !== null) {
      initialImages = {
        uid: "profile_image",
        name: "profile_image.png",
        status: "done",
        url: getDetailProfileResult.imageProfile,
      };
    }
  }

  // jika sudah selesai loading & dapat result
  if (getUpdateProfileResult) {
    localStorage.setItem("setAlertSuccessProfile", true);
    window.location.replace("/daftar-jual/semua");
  } else if (getUpdateProfileError) {
    localStorage.setItem("setAlertErrorProfile", true);
    window.location.reload();
  }

  return (
    <>
      {getUpdateProfileLoading && <LoadingWithBackground />}
      {showAlert === "Error" && <Alert message={"Edit profil gagal disimpan !"} type="error" closable className="custom-alert" />}
      {showAlert === "Lengkapi-Profile" && <Alert message={"Mohon lengkapi profil sebelum menjual barang !"} type="error" closable className="custom-alert" />}
      {getDetailProfileLoading ? (
        <LoadingWithBackground />
      ) : getDetailProfileResult ? (
        <div className="flex flex-col gap-6 sm:flex-row w-full justify-center px-4 pb-6 sm:px-0 sm:py-10 relative">
          {/* start tampilan atas page (tanda back) */}
          {isDesktop ? (
            <Link to="/daftar-jual/semua">
              <img src="/assets/images/fi_arrow-left.svg" alt="<" className="absolute sm:left-12 lg:left-52" />
            </Link>
          ) : (
            <div className="py-4 flex flex-row items-center gap-4 relative">
              <Link to="/daftar-jual/semua" className="absolute left-0">
                <img src="/assets/images/fi_arrow-left.svg" alt="<" />
              </Link>
              <span className="grow text-center font-medium text-sm">Lengkapi Info Akun</span>
            </div>
          )}
          {/* end tampilan atas page (tanda back) */}

          {/* start tampilan form */}
          <Form layout="vertical" form={formInfoProfile} onFinish={handleSubmit} className="w-full sm:w-2/3 lg:w-2/5" initialValues={initialValues}>
            <Form.Item
              name="imageProfil"
              getValueFromEvent={(e) => {
                return e.file;
              }}
              initialValue={initialImages}
              rules={[
                {
                  required: true,
                  message: "Harap masukkan foto !",
                },
              ]}
            >
              <Upload id="imageProfil" listType="picture-card" fileList={fileList} onPreview={handlePreview} onChange={handleChange} maxCount={1} beforeUpload={() => false} className="upload-img">
                {fileList.length >= 1 ? null : (
                  <CameraOutlined
                    style={{
                      fontSize: 24,
                      color: "#7126B5",
                      backgroundColor: "#E2D4F0",
                      width: "100%",
                      height: "100%",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: "12px",
                    }}
                  />
                )}
              </Upload>
            </Form.Item>
            <Form.Item
              label="Nama"
              name="fullname"
              rules={[
                {
                  required: true,
                  message: "Harap isi nama !",
                },
              ]}
            >
              <input
                id="fullname"
                name="namaUser"
                placeholder="Nama"
                className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none"
              />
            </Form.Item>
            <Form.Item
              name="city"
              label="Kota"
              rules={[
                {
                  required: true,
                  message: "Harap pilih kota !",
                },
              ]}
            >
              <Select
                showSearch
                showArrow
                suffixIcon={<img src="/assets/images/fi_chevron-down.svg" alt="v" />}
                placeholder="Pilih Kota"
                optionFilterProp="children"
                filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
              >
                <Option value="medan">Medan</Option>
                <Option value="binjai">Binjai</Option>
                <Option value="tebing">Tebing Tinggi</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Alamat"
              name="address"
              rules={[
                {
                  required: true,
                  message: "Harap isi alamat !",
                },
              ]}
            >
              <textarea
                id="address"
                maxLength={100}
                name="alamatUser"
                placeholder="Contoh: Jalan Ikan Hiu 33"
                className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full resize-none hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none"
              ></textarea>
            </Form.Item>
            <Form.Item
              label="No Handphone"
              name="phone"
              rules={[
                {
                  required: true,
                  message: "Harap isi nomor hp !",
                },
              ]}
            >
              <input
                id="phone"
                name="nomorUser"
                placeholder="contoh: +628123456789"
                className="px-4 py-3 rounded-2xl border border-neutral02 text-body-14 w-full hover:border-primary-darkblue04 focus:border-2 focus:border-primary-darkblue04 focus:outline-none"
              />
            </Form.Item>
            <Form.Item>
              <button key="submit" htmlType="submit" className="text-body-14 leading-5 bg-primary-darkblue04 hover:bg-primary-darkblue05 text-neutral01 font-medium px-6 py-3 rounded-2xl w-full">
                Simpan
              </button>
              <button onClick={logout} className="text-body-14 leading-5 mt-6 border-2 border-primary-darkblue05 hover:bg-primary-darkblue04 hover:text-white text-primary-darkblue05 font-semibold px-6 py-3 rounded-2xl w-full">
                Logout
              </button>
            </Form.Item>
          </Form>
          {/* end tampilan form */}

          {/* start modal untuk preview foto */}
          <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={handleCancel}>
            <img alt="Gambar Profile" style={{ width: "100%" }} src={previewImage} />
          </Modal>
          {/* end modal untuk preview foto */}
        </div>
      ) : (
        <div className="w-screen h-screen flex justify-center items-center">
          <Empty />
        </div>
      )}
    </>
  );
}

export default InfoProfilPage;
