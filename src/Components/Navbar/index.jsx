import React, { useState } from "react";
import Notifikasi from "../Notifikasi";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { getUserProfile } from "../../stores/actions/profile";

function Navbar() {
  const dispatch = useDispatch();
  const { getDetailProfileResult } = useSelector((state) => state.profileReducer);

  const [isUser, setIsUser] = useState(false);
  const [visibleNotification, setVisibleNotification] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);

  const location = useLocation();
  const navigate = useNavigate();

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1024px)",
  });

  const isTabletOrMobile = useMediaQuery({ query: "(max-width:640px)" });

  const handleNotification = () => {
    setVisibleNotification(!visibleNotification);
  };

  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  // untuk simpan gambar profile user untuk ditampilkan di halaman profile nanti
  if (getDetailProfileResult.imageProfile) {
    const initialImages = {
      uid: "profile_image",
      name: "profile_image.png",
      status: "done",
      url: getDetailProfileResult.imageProfile,
    };
    localStorage.setItem("userImage", JSON.stringify(initialImages));
  }

  useEffect(() => {
    if (localStorage.getItem("userLocal") === null) {
      return setIsUser(false);
    } else {
      dispatch(getUserProfile(accessToken)); // mengambil detail user profile
      return setIsUser(true);
    }
  }, [dispatch, accessToken]);

  const logout = () => {
    localStorage.removeItem("userLocal");
    localStorage.removeItem("userImage");
    setIsUser(false);
    navigate("/login");
  };

  return (
    <>
      {isDesktopOrLaptop && (
        <>
          <nav className="flex flex-row items-center justify-between px-28 py-4 w-full h-full shadow">
            <div className="flex flex-row items-center">
              <Link to={"/"}>
                <img src="/assets/images/tilas.jpg" className="h-12 w-14 object-cover" alt="logo" />
              </Link>
              <form action="/" method="GET">
                <div className="relative focus-within:text-gray-600 text-gray-400 ml-6 ">
                  <input
                    type="search"
                    name="searchProduct"
                    className="py-2 text-sm bg-[#EEEEEE] text-white rounded-lg focus:shadow-xl focus:text-gray-900 w-96 border-transparent focus:outline-none focus:border-primary-darkblue04 focus:ring-1 focus:ring-primary-darkblue04 "
                    placeholder="Cari di sini ..."
                    autoComplete="off"
                    required
                  />
                  <span className="absolute inset-y-0 right-0 flex items-center pr-2 ">
                    <button type="submit" className="p-1 focus:outline-none focus:shadow-outline">
                      <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" className="w-6 h-6">
                        <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                      </svg>
                    </button>
                  </span>
                </div>
              </form>
            </div>

            <div className="flex justify-center items-center">
              {isUser ? (
                <div className="flex items-center">
                  <Link to={"/daftar-jual/semua"}>
                    {location.pathname === "/daftar-jual/semua" || location.pathname === "/daftar-jual/diminati" || location.pathname === "/daftar-jual/terjual" ? (
                      <img src="/assets/images/fi_list_active.svg" alt="icon list" width={20} className="mr-5" />
                    ) : (
                      <img src="/assets/images/fi_list.svg" alt="icon list" width={20} className="mr-5" />
                    )}
                  </Link>
                  <div onClick={handleNotification} className="hover:cursor-pointer">
                    {visibleNotification ? <img src="/assets/images/fi_bell_active.svg" alt="icon bel" width={20} className="mr-5" /> : <img src="/assets/images/fi_bell.svg" alt="icon bel" width={20} className="mr-5" />}
                  </div>
                  <Link to={"../info-profil"}>
                    <img src="/assets/images/fi_user.svg" alt="icon user" width={20} />
                  </Link>
                </div>
              ) : (
                <Link to={"/login"}>
                  <button className="bg-primary-darkblue04 h-9 px-3 rounded-md flex items-center">
                    <img src="/assets/images/fi_log-in.svg" alt="icon login" width={20} />
                    <p className="pl-1 pt-3 text-white">Masuk</p>
                  </button>
                </Link>
              )}
            </div>
          </nav>

          {visibleNotification && <Notifikasi />}
        </>
      )}

      {isTabletOrMobile && (
        <>
          {location.pathname === "/" ? (
            <form action="/" method="GET">
              <div className="absolute top-5 right-6 focus-within:text-gray-600 text-gray-400  z-20">
                <input
                  type="search"
                  name="searchProduct"
                  className="py-3 w-[75vw] text-sm bg-white text-white rounded-lg focus:shadow-xl focus:text-gray-900 border-transparent focus:outline-none focus:border-primary-darkblue04 focus:ring-1 focus:ring-primary-darkblue04 "
                  placeholder="Cari di sini ..."
                  autoComplete="off"
                  required
                />
                <span className="absolute inset-y-0 right-0 flex items-center pr-2 ">
                  <button type="submit" className="p-1 focus:outline-none focus:shadow-outline">
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" className="w-6 h-6">
                      <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                    </svg>
                  </button>
                </span>
              </div>
            </form>
          ) : (
            ""
          )}
          {showSidebar ? (
            <div className="flex z-50 fixed justify-between">
              <Link to={"/"}>
                <img src="/assets/images/tilas2.jpg" alt="second hand" className="ml-6 mt-6 w-28 object-cover" />
              </Link>
              <button className="flex text-4xl text-white items-center cursor-pointer fixed left-6 top-6 z-50" onClick={() => setShowSidebar(!showSidebar)}>
                <p className="px-2 text-lg z-50 fixed text-black font-semibold ml-32 mt-12">X</p>
              </button>
            </div>
          ) : (
            <img src="/assets/images/fi_menu.svg" alt="menu icon" onClick={() => setShowSidebar(!showSidebar)} className="absolute z-30 flex items-center cursor-pointer left-4 top-6 bg-white px-2 py-2 rounded-xl" />
          )}
          <div className="flex w-full">
            <div className={`top-0 left-0 w-1/2 bg-white pl-6 text-white fixed h-full z-40  ease-in-out duration-300 ${showSidebar ? "translate-y-0 " : "-translate-x-full"}`}>
              {isUser ? (
                <div className="my-16">
                  <Link to={"/notifikasi"}>
                    <p className="text-lg font-medium text-black">Notifikasi</p>
                  </Link>
                  <Link to={"/daftar-jual/semua"}>
                    <p className="text-lg font-medium text-black">Daftar Jual</p>
                  </Link>
                  <Link to={"/info-profil"}>
                    <p className="text-lg font-medium text-black">Akun Saya</p>
                  </Link>
                  <button className="bg-primary-darkblue04 py-2 px-4 text-lg rounded-lg font-medium mt-2" onClick={logout}>
                    Log Out
                  </button>
                </div>
              ) : (
                <Link to={"/login"}>
                  <button className="bg-primary-darkblue04 h-9 px-3 mb-5 rounded-md flex items-center mt-20">
                    <img src="/assets/images/fi_log-in.svg" alt="icon login" width={20} />
                    <p className="pl-1 pt-3 text-white">Masuk</p>
                  </button>
                </Link>
              )}
            </div>
            <div className={`bg-black opacity-60 w-1/2 h-full  right-0 fixed z-40  ${showSidebar ? "translate-x-0 " : "translate-y-full"}`}></div>
          </div>
        </>
      )}
    </>
  );
}

export default Navbar;
