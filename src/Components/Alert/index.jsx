import React from "react";
import { Alert } from "antd";

function AlertComponent({ message }) {
  return <Alert message={message} type="success" closable />;
}

export default AlertComponent;
