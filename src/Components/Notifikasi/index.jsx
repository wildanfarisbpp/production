import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getNotification, editNotification } from "../../stores/actions/notification";
import moment from "moment";
import LoadingWithBackground from "../Loading/withBackground";

function Notifikasi() {
  const [isRead, setIsRead] = useState(false);
  const dispatch = useDispatch();
  const token = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  const { getNotificationResult, getNotificationLoading, getEditNotificationResult } = useSelector((state) => state.notificationReducer);

  useEffect(() => {
    dispatch(getNotification({ token }));
  }, [dispatch]);

  return (
    <>
      <div className="flex flex-col w-96 max-h-[50vh] overflow-y-auto ml-auto mr-28 absolute right-0 top-16 bg-white shadow-xl z-10 rounded-xl ">
        {getNotificationResult &&
          getNotificationResult.map((notifikasi) => {
            return notifikasi.title === "Penawaran produk" ? (
              <Link to={`/info-penawar/${notifikasi.offerId}`}>
                <div className="flex my-auto px-6 py-3 hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                  <img src={notifikasi.image1} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                  <div className="flex flex-col ">
                    <span className="text-body-10 ml-4 text-neutral03 ">{notifikasi.title}</span>
                    <span className="text-body-14 ml-4 text-black">{notifikasi.productName}</span>
                    <span className="text-body-14 ml-4 text-black">
                      Rp{" "}
                      {parseInt(notifikasi.price)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                        .toString()}
                    </span>
                    <span className="text-body-14 ml-4 text-black ">
                      Ditawar Rp Rp{" "}
                      {parseInt(notifikasi.priceNegotiated)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                        .toString()}
                    </span>
                  </div>
                  <span className="text-body-10 text-neutral03 ml-auto mr-2">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                  <span className={notifikasi.isRead === false ? `w-2 h-2 bg-alert-danger rounded-full mt-1 mr-4` : `hidden`}></span>
                </div>
              </Link>
            ) : (
              notifikasi.title === "Produk berhasil diterbitkan" && (
                <Link to={`/produk-seller/${notifikasi.productId}`}>
                  <div className="flex my-auto px-6 py-3 hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                    <img src={notifikasi.image1} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                    <div className="flex flex-col ">
                      <span className="text-body-10 ml-4 text-neutral03 ">{notifikasi.title}</span>
                      <span className="text-body-14 ml-4 text-black">{notifikasi.productName}</span>
                      <span className="text-body-14 ml-4 text-black">
                        Rp{" "}
                        {parseInt(notifikasi.price)
                          .toFixed(2)
                          .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                          .toString()}
                      </span>
                    </div>
                    <span className="text-body-10 text-neutral03 ml-auto mr-2">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                    <span className={notifikasi.isRead === false ? `w-2 h-2 bg-alert-danger rounded-full mt-1 mr-4` : `hidden`}></span>
                  </div>
                </Link>
              )
            );
          })}

        {getNotificationResult &&
          getNotificationResult.map((notifikasi) => {
            return notifikasi.info === "penawaran anda telah diterima" ? (
              <div className="flex my-auto px-6 py-3 hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                <img src={notifikasi.image1} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                <div className="flex flex-col ">
                  <span className="text-body-10 ml-4 text-neutral03 ">{notifikasi.title}</span>
                  <span className="text-body-14 ml-4 text-black">{notifikasi.productName}</span>
                  <span className="text-body-14 ml-4 text-black">
                    Rp{" "}
                    {parseInt(notifikasi.price)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                      .toString()}
                  </span>
                  <span className="text-body-14 ml-4 text-black ">
                    Ditawar Rp Rp{" "}
                    {parseInt(notifikasi.priceNegotiated)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                      .toString()}
                  </span>
                </div>
                <span className="text-body-10 text-neutral03 ml-auto mr-2">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                <span className={notifikasi.isRead === false ? `w-2 h-2 bg-alert-danger rounded-full mt-1 mr-4` : `hidden`}></span>
              </div>
            ) : (
              notifikasi.info === "penawaran anda telah ditolak" && (
                <div className="flex my-auto px-6 py-3 hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                  <img src={notifikasi.image1} alt="foto pembeli" className="w-12 h-12 object-cover rounded-lg" />
                  <div className="flex flex-col ">
                    <span className="text-body-10 ml-4 text-neutral03 ">{notifikasi.title}</span>
                    <span className="text-body-14 ml-4 text-black">{notifikasi.productName}</span>
                    <span className="text-body-14 ml-4 text-black">
                      Rp{" "}
                      {parseInt(notifikasi.price)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                        .toString()}
                    </span>
                  </div>
                  <span className="text-body-10 text-neutral03 ml-auto mr-2">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                  <span className={notifikasi.isRead === false ? `w-2 h-2 bg-alert-danger rounded-full mt-1 mr-4` : `hidden`}></span>
                </div>
              )
            );
          })}
      </div>
    </>
  );
}

export default Notifikasi;
