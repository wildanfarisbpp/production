import { Spin } from "antd"

function LoadingWithBackground(){
    return (
        <div id="bg-loading" className="fixed z-50 bg-white bg-opacity-90 w-screen h-screen flex justify-center items-center">
            <Spin tip="Loading..." size="large" spinning="true"></Spin>
        </div>
    )
}
export default LoadingWithBackground