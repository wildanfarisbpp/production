import axios from "axios";

export const ADD_OFFER = "ADD_OFFER";
export const GET_OFFER_BY_ID = "GET_OFFER_BY_ID";
export const EDIT_STATUS_OFFER_ISACCEPTED = "EDIT_STATUS_OFFER_ISACCEPTED";
export const EDIT_STATUS_OFFER_ISREJECTED = "EDIT_STATUS_OFFER_ISREJECTED";
export const EDIT_STATUS_OFFER_ISSOLD = "EDIT_STATUS_OFFER_ISSOLD";
export const EDIT_STATUS_OFFER_ISNOTSOLD = "EDIT_STATUS_OFFER_ISNOTSOLD";

export const addOffer = (id, values, token) => {
  return async (dispatch) => {
    try {
      //Loading
      dispatch({
        type: ADD_OFFER,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      //post API
      const resAddOffer = await axios.post(`https://secondhand-restapi.herokuapp.com/api/offer/add/${id}`, values, {
        headers: {
          accept: "*/*",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      dispatch({
        type: ADD_OFFER,
        payload: {
          loading: false,
          data: resAddOffer,
          errorMessage: false,
        },
      });
      console.log("Response", resAddOffer);
    } catch (error) {
      dispatch({
        type: ADD_OFFER,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const getOfferById = ({ id, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: GET_OFFER_BY_ID,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      console.log("Request id", id);
      const resGetOffer = await axios.get(`https://secondhand-restapi.herokuapp.com/api/offer/show-offer/${id}`, { headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resGetOffer);
      dispatch({
        type: GET_OFFER_BY_ID,
        payload: {
          loading: false,
          data: resGetOffer.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: GET_OFFER_BY_ID,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const editStatusOfferIsAccepted = ({ id, status, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      const resEditOffer = await axios({ method: "PUT", url: `https://secondhand-restapi.herokuapp.com/api/offer/update/${id}/${status}`, headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response edit status offer", resEditOffer);
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: false,
          data: resEditOffer.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const editStatusOfferIsRejected = ({ id, status, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      const resEditOffer = await axios({ method: "PUT", url: `https://secondhand-restapi.herokuapp.com/api/offer/update/${id}/${status}`, headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response edit status offer", resEditOffer);
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: false,
          data: resEditOffer.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: EDIT_STATUS_OFFER_ISACCEPTED,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const editStatusOfferIsSold = ({ id, status, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: EDIT_STATUS_OFFER_ISSOLD,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      const resEditOfferIsSold = await axios({ method: "PUT", url: `https://secondhand-restapi.herokuapp.com/api/offer/update/isSold/${id}/${status}`, headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response edit status offer", resEditOfferIsSold);
      dispatch({
        type: EDIT_STATUS_OFFER_ISSOLD,
        payload: {
          loading: false,
          data: resEditOfferIsSold.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: EDIT_STATUS_OFFER_ISSOLD,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const editStatusOfferIsNotSold = ({ id, status, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: EDIT_STATUS_OFFER_ISNOTSOLD,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      const resEditOfferIsNotSold = await axios({ method: "PUT", url: `https://secondhand-restapi.herokuapp.com/api/offer/update/isSold/${id}/${status}`, headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response edit status offer", resEditOfferIsNotSold);
      dispatch({
        type: EDIT_STATUS_OFFER_ISNOTSOLD,
        payload: {
          loading: false,
          data: resEditOfferIsNotSold.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: EDIT_STATUS_OFFER_ISNOTSOLD,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};
