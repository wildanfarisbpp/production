import axios from "axios";

export const ADD_PRODUCT = "ADD_PRODUCT"
export const GET_PRODUCT_BY_ID = "GET_PRODUCT_BY_ID"
export const EDIT_PRODUCT_BY_ID = "EDIT_PRODUCT_BY_ID"
export const DELETE_PRODUCT_BY_ID = "DELETE_PRODUCT_BY_ID"
export const GET_ALL_PRODUCT = "GET_ALL_PRODUCT"
export const GET_CATEGORY = "GET_CATEGORY"

export const getAllProduct = ({search, category, page}) => {
    return async(dispatch) => {
        try {
            dispatch({
                type: GET_ALL_PRODUCT,
                payload: {
                    loading: true,
                    data: false,
                    currentPage: false,
                    totalPages: false,
                    errorMessage: false
                }
            })
            if (search !== null){
              const resSearchProduct = await axios.get(`https://secondhand-restapi.herokuapp.com/api/homepage/get-product?productName=${search}`)
              console.log(resSearchProduct)
              dispatch({
                  type: GET_ALL_PRODUCT,
                  payload: {
                      loading: false,
                      data: resSearchProduct.data.products,
                      currentPage: resSearchProduct.data.currentPage,
                      totalPages: resSearchProduct.data.totalPages,
                      errorMessage: false
                  }
              })
            }else if (category !=='semua'){
              const resCategoryProduct = await axios.get(`https://secondhand-restapi.herokuapp.com/api/homepage/get-product?category=${category}&page=${page}`)
              console.log(resCategoryProduct)
              dispatch({
                  type: GET_ALL_PRODUCT,
                  payload: {
                      loading: false,
                      data: resCategoryProduct.data.products,
                      currentPage: resCategoryProduct.data.currentPage,
                      totalPages: resCategoryProduct.data.totalPages,
                      errorMessage: false
                  }
              })
            } else {
              const resAllProduct = await axios.get(`https://secondhand-restapi.herokuapp.com/api/homepage/get-product?page=${page}`)
              console.log(resAllProduct)
              dispatch({
                  type: GET_ALL_PRODUCT,
                  payload: {
                      loading: false,
                      data: resAllProduct.data.products,
                      currentPage: resAllProduct.data.currentPage,
                      totalPages: resAllProduct.data.totalPages,
                      errorMessage: false
                  }
              })

            }
        } catch (error) {
            console.log(error)
            dispatch({
                type: GET_ALL_PRODUCT,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
    }
}

export const getCategory = () => {
  return async (dispatch) => {
    try {
      const resGetCategory = await axios.get(`https://secondhand-restapi.herokuapp.com/api/homepage/list-category`);
      console.log("Response", resGetCategory);
      dispatch({
        type: GET_CATEGORY,
        payload: {
          loading: false,
          data: resGetCategory.data,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: GET_CATEGORY,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};


export const addProduct = ({ status, values, token }) => {
  return async (dispatch) => {
    try {
      // loading
      dispatch({
        type: ADD_PRODUCT,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      console.log('Request', status)
            let formData = new FormData();
            let isiBodyData = { 
                "productName": values.productName,
                "categoryId": values.category,
                "price": values.price,
                "description": values.description
            }
            formData.append('addJson', JSON.stringify(isiBodyData))
            if (values.image !== undefined) {
                for(let i=1; i<=values.image.length; i++){
                    formData.append('image', values.image[i-1].originFileObj)
                }
            }
            const resAddProduct = await axios.post(`https://secondhand-restapi.herokuapp.com/api/product/add/${status}`, formData,
                { 
                headers: {
                    "accept": '*/*',
                    "Content-Type": 'multipart/form-data',
                    "Authorization" : `Bearer ${token}`
                } })
            console.log('Response', resAddProduct)
      dispatch({
        type: ADD_PRODUCT,
        payload: {
          loading: false,
          data: resAddProduct,
          errorMessage: false,
        },
      });
    } catch (error) {
      dispatch({
        type: ADD_PRODUCT,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const getProductById = ({id, token}) => {
    return async(dispatch) => {
        try {
            // loading
            dispatch({
                type: GET_PRODUCT_BY_ID,
                payload: {
                    loading: true,
                    data: false,
                    errorMessage: false
                }
            })
            console.log('Request id', id)
            const resGetProduct = await axios.get(`https://secondhand-restapi.herokuapp.com/api/product/${id}`, { headers: { 'accept': '*/*', "Authorization" : `Bearer ${token}` } })
            console.log('Response', resGetProduct)
            dispatch({
                type: GET_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: resGetProduct.data,
                    errorMessage: false
                }
            })
        } catch (error){
            dispatch({
                type: GET_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
  };
};

export const editProductById = ({id, values, token}) => {
    return async(dispatch) => {
        try {
            // loading
            dispatch({
                type: EDIT_PRODUCT_BY_ID,
                payload: {
                    loading: true,
                    data: false,
                    errorMessage: false
                }
            })
            console.log('Request', values)
            let formData = new FormData();
            let isiBodyData = { 
                "productName": values.productName,
                "price": values.price,
                "description": values.description
            }
            formData.append('updateJson', JSON.stringify(isiBodyData))
            for(let i=1; i<=values.image.length; i++){
                if (values.image[i-1].originFileObj !== undefined){
                    formData.append('image', values.image[i-1].originFileObj)
                    console.log(values.image[i-1].originFileObj)
                } else {
                    formData.append('image', values.image[i-1].url)
                    console.log(values.image[i-1].url)
                }
            }
            const resEditProduct = await axios.put(`https://secondhand-restapi.herokuapp.com/api/product/update/${id}`, formData,
                { 
                headers: {
                    "accept": '*/*',
                    "Content-Type": 'multipart/form-data',
                    "Authorization" : `Bearer ${token}`
                } })
            console.log('Response', resEditProduct)
            dispatch({
                type: EDIT_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: resEditProduct,
                    errorMessage: false
                }
            })
        } catch (error){
            dispatch({
                type: EDIT_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
  };
};

export const deleteProductById = ({id, token}) => {
    return async(dispatch) => {
        try {
            // loading
            dispatch({
                type: DELETE_PRODUCT_BY_ID,
                payload: {
                    loading: true,
                    data: false,
                    errorMessage: false
                }
            })
            console.log('Request id', id)
            const resDeleteProduct = await axios.delete(
                `https://secondhand-restapi.herokuapp.com/api/product/delete/${id}`, 
                { 
                headers: {
                    "accept": '*/*',
                    "Authorization" : `Bearer ${token}`
                } })
            console.log('Response', resDeleteProduct)
            dispatch({
                type: DELETE_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: resDeleteProduct,
                    errorMessage: false
                }
            })
        } catch (error){
            dispatch({
                type: DELETE_PRODUCT_BY_ID,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
    }
}
