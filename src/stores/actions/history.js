import axios from "axios";

export const GET_PRODUCT_SELLER = "PRODUCT_SELLER";
export const GET_PRODUCT_WISHLIST = "PRODUCT_WISHLIST";
export const GET_PRODUCT_SOLD = "PRODUCT_SOLD";

export const productSeller = ({ token }) => {
  return async (dispatch) => {
    try {
      //  LOADING
      dispatch({
        type: GET_PRODUCT_SELLER,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      // GET DATA PRODUCT SELLER
      const resGetProduct = await axios.get(`https://secondhand-restapi.herokuapp.com/api/history/product-user`, { headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resGetProduct);
      dispatch({
        type: GET_PRODUCT_SELLER,
        payload: {
          loading: false,
          data: resGetProduct.data,
          errorMessage: false,
        },
      });
      //   CATCH ERROR WHILE FETCH API
    } catch (error) {
      dispatch({
        type: GET_PRODUCT_SELLER,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const productWishlist = ({ token }) => {
  return async (dispatch) => {
    try {
      //  LOADING
      dispatch({
        type: GET_PRODUCT_WISHLIST,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      // GET DATA PRODUCT WISHLIST
      const resGetWishlist = await axios.get(`https://secondhand-restapi.herokuapp.com/api/history/product-wishlist`, { headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resGetWishlist);
      dispatch({
        type: GET_PRODUCT_WISHLIST,
        payload: {
          loading: false,
          data: resGetWishlist.data,
          errorMessage: false,
        },
      });
      //   CATCH ERROR WHILE FETCH API
    } catch (error) {
      dispatch({
        type: GET_PRODUCT_WISHLIST,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const productSold = ({ token }) => {
  return async (dispatch) => {
    try {
      //  LOADING
      dispatch({
        type: GET_PRODUCT_SOLD,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      // GET DATA PRODUCT WISHLIST
      const resGetSoldProduct = await axios.get("https://secondhand-restapi.herokuapp.com/api/history/product-sold", { headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resGetSoldProduct);
      dispatch({
        type: GET_PRODUCT_SOLD,
        payload: {
          loading: false,
          data: resGetSoldProduct.data,
          errorMessage: false,
        },
      });
      //   CATCH ERROR WHILE FETCH API
    } catch (error) {
      dispatch({
        type: GET_PRODUCT_SOLD,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};
