import { ADD_OFFER, GET_OFFER_BY_ID, EDIT_STATUS_OFFER_ISACCEPTED, EDIT_STATUS_OFFER_ISSOLD, EDIT_STATUS_OFFER_ISREJECTED, EDIT_STATUS_OFFER_ISNOTSOLD } from "../actions/offer";

const initialState = {
  getAddOfferResult: false,
  getAddOfferLoading: false,
  getAddOfferError: false,

  getOfferResult: false,
  getOfferLoading: false,
  getOfferError: false,

  getEditStatusOfferIsAcceptedResult: false,
  getEditStatusOfferIsAcceptedLoading: false,
  getEditStatusOfferIsAcceptedError: false,

  getEditStatusOfferIsRejectedResult: false,
  getEditStatusOfferIsRejectedLoading: false,
  getEditStatusOfferIsRejectedError: false,

  getEditStatusOfferIsSoldResult: false,
  getEditStatusOfferIsSoldLoading: false,
  getEditStatusOfferIsSoldError: false,

  getEditStatusOfferisNotSoldResult: false,
  getEditStatusOfferisNotSoldLoading: false,
  getEditStatusOfferisNotSoldError: false,
};

const offerReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_OFFER:
      return {
        ...state,
        getAddOfferLoading: action.payload.loading,
        getAddOfferResult: action.payload.data,
        getAddOfferError: action.payload.errorMessage,
      };
    case GET_OFFER_BY_ID:
      return {
        ...state,
        getOfferResult: action.payload.data,
        getOfferLoading: action.payload.loading,
        getOfferError: action.payload.errorMessage,
      };
    case EDIT_STATUS_OFFER_ISACCEPTED:
      return {
        ...state,
        getEditStatusOfferIsAcceptedResult: action.payload.data,
        getEditStatusOfferIsAcceptedLoading: action.payload.loading,
        getEditStatusOfferIsAcceptedError: action.payload.errorMessage,
      };
    case EDIT_STATUS_OFFER_ISREJECTED:
      return {
        ...state,
        getEditStatusOfferIsRejectedResult: action.payload.data,
        getEditStatusOfferIsRejectedLoading: action.payload.loading,
        getEditStatusOfferIsRejectedError: action.payload.errorMessage,
      };
    case EDIT_STATUS_OFFER_ISSOLD:
      return {
        ...state,
        getEditStatusOfferIsSoldResult: action.payload.data,
        getEditStatusOfferIsSoldLoading: action.payload.loading,
        getEditStatusOfferIsSoldError: action.payload.errorMessage,
      };
    case EDIT_STATUS_OFFER_ISNOTSOLD:
      return {
        ...state,
        getEditStatusOfferisNotSoldResult: action.payload.data,
        getEditStatusOfferisNotSoldLoading: action.payload.loading,
        getEditStatusOfferisNotSoldError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export default offerReducer;
