import { combineReducers } from "redux";
import authReducer from "./auth";
import profileReducer from "./profile";
import productReducer from "./product";
import historyReducer from "./history";
import offerReducer from "./offer";
import notificationReducer from "./notification";

const rootReducers = combineReducers({
  profileReducer,
  productReducer,
  authReducer,
  historyReducer,
  offerReducer,
  notificationReducer,
});

export default rootReducers;
