import { 
    GET_ALL_PRODUCT,
    GET_CATEGORY,
    ADD_PRODUCT, 
    GET_PRODUCT_BY_ID, 
    EDIT_PRODUCT_BY_ID, 
    DELETE_PRODUCT_BY_ID
} from "../actions/product";

const initialState = {
    getAllProductResult: false, getAllProductLoading: false, getAllProductError: false,
    getAllProductCurrentPage: false, getAllProductTotalPages: false,
    getCategoryResult: false, getCategoryLoading: false, getCategoryError: false,
    getAddProductResult: false, getAddProductLoading: false, getAddProductError: false,
    getDetailProductResult: false, getDetailProductLoading: false, getDetailProductError: false,
    getEditProductResult: false, getEditProductLoading: false, getEditProductError: false,
    getDeleteProductResult: false, getDeleteProductLoading: false, getDeleteProductError: false
};

const productReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_ALL_PRODUCT:
            return {...state,
                getAllProductResult: action.payload.data,
                getAllProductLoading: action.payload.loading,
                getAllProductCurrentPage: action.payload.currentPage,
                getAllProductTotalPages: action.payload.totalPages,
                getAllProductError: action.payload.errorMessage
            };
        case GET_CATEGORY:
            return {...state,
                getCategoryResult: action.payload.data,
                getCategoryLoading: action.payload.loading,
                getCategoryError: action.payload.errorMessage
            };
        case ADD_PRODUCT:
            return { ...state,
                getAddProductResult: action.payload.data,
                getAddProductLoading: action.payload.loading,
                getAddProductError: action.payload.errorMessage
            };
        case GET_PRODUCT_BY_ID:
            return { ...state,
                getDetailProductResult: action.payload.data,
                getDetailProductLoading: action.payload.loading,
                getDetailProductError: action.payload.errorMessage
            };
        case EDIT_PRODUCT_BY_ID:
            return { ...state,
                getEditProductResult: action.payload.data,
                getEditProductLoading: action.payload.loading,
                getEditProductError: action.payload.errorMessage
            };
        case DELETE_PRODUCT_BY_ID:
            return { ...state,
                getDeleteProductResult: action.payload.data,
                getDeleteProductLoading: action.payload.loading,
                getDeleteProductError: action.payload.errorMessage
            };
        default: return state;
    }
}

export default productReducer