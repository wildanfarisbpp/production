import { BrowserRouter, Routes, Route } from "react-router-dom";
import DaftarJualPage from "./pages/DaftarJualPage";
import HomePage from "./pages/HomePage";
import InfoPenawarPage from "./pages/InfoPenawarPage";
import InfoProdukPage from "./pages/InfoProdukPage";
import EditProdukPage from "./pages/InfoProdukPage/edit";
import InfoProfilPage from "./pages/InfoProfilPage";
import LoginPage from "./pages/LoginPage";
import ProdukPage from "./pages/ProdukPage";
import ProdukPageSeller from "./pages/ProdukPage/seller";
import RegisterPage from "./pages/RegisterPage";
import React from "react";
import NotifikasiPage from "./pages/NotifikasiPage";
import ProdukSemua from "./pages/DaftarJualPage/semua";
import ProdukDiminati from "./pages/DaftarJualPage/diminati";
import ProdukTerjual from "./pages/DaftarJualPage/terjual";
import ProdukPageBuyer from "./pages/ProdukPageBuyer";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="login" element={<LoginPage />} />
          <Route path="register" element={<RegisterPage />} />
          <Route path="info-profil" element={<InfoProfilPage />} />
          <Route path="info-produk" element={<InfoProdukPage />} />
          <Route path="edit-produk/:id" element={<EditProdukPage />} />
          <Route path="produk-seller/:id" element={<ProdukPageSeller />} />
          <Route path="produk-buyer/:id" element={<ProdukPageBuyer />} />
          <Route path="info-penawar/:id" element={<InfoPenawarPage />} />
          <Route path="produk" element={<ProdukPage />} />
          <Route path="daftar-jual" element={<DaftarJualPage />}>
            <Route path="semua" element={<ProdukSemua />} />
            <Route path="diminati" element={<ProdukDiminati />} />
            <Route path="terjual" element={<ProdukTerjual />} />
          </Route>
          <Route path="notifikasi" element={<NotifikasiPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
