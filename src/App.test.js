import { render, screen } from "@testing-library/react";
import LoadingWithBackground from "./Components/Loading";
import HomePage from "./pages/HomePage";

test("should render a spin loading", () => {
  render(<LoadingWithBackground />);
  const loadingElement = screen.getByLabelText("loading");
  expect(loadingElement).toBeInTheDocument();
});
